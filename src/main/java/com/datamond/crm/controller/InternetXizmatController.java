package com.datamond.crm.controller;


import com.datamond.crm.constant.*;
import com.datamond.crm.dto.dashboard.InternetDtoForDashboard;
import com.datamond.crm.dto.dashboard.SmsDtoForDashboard;
import com.datamond.crm.entity.*;
import com.datamond.crm.repository.*;
import com.datamond.crm.service.InternetXizmatService;
import com.datamond.crm.service.SmsXizmatService;
import com.datamond.crm.utils.AuthUtils;
import com.datamond.crm.utils.Breadcrumb;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class InternetXizmatController {

    private Logger logger = LogManager.getLogger(InternetXizmatController.class);

    private InternetXizmatService internetXizmatService;

    @Autowired
    private OperatorRepository operatorRepository;

    @Autowired
    private RegionRepository regionRepository;

    @Autowired
    private SmsRepository smsRepository;

    @Autowired
    private InternetRepository internetRepository ;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    public InternetXizmatController(InternetXizmatService internetXizmatService) {
        this.internetXizmatService = internetXizmatService;
    }

    @RequestMapping(value = {Urls.ModelInterServiceList})
    public String getXizmatList(Model model) {
        logger.info("[employee control] model: " + model);
        model.addAttribute("breadcrumb", getBreadcrumbForService("label.list"));
        model.addAttribute("url_edit", Urls.ModelInterServiceEdit);
        model.addAttribute("url_view", Urls.ModelInterServiceView);
        model.addAttribute("url_ajax", Urls.ModelInterServiceListDataTable);
        model.addAttribute("url_add", Urls.ModelInterServiceCreate);
        model.addAttribute("url_delete", Urls.ModelInterServiceDelete);
        logger.info("[organization control] model: " + model);


        List<InternetDtoForDashboard> xizmats = internetXizmatService.getData(AuthUtils.getUserId(), AuthUtils.getUserEntity().getBranchId());
        model.addAttribute("names", getNames(xizmats));
        model.addAttribute("counts", getCounters(xizmats));
        model.addAttribute("colors", getColors(xizmats));


        List<InternetDtoForDashboard> xizmatlar = internetXizmatService.getDataForComing(AuthUtils.getUserId(), AuthUtils.getUserEntity().getBranchId());
        model.addAttribute("date", "Oxirgi 30 kun");
        model.addAttribute("counter_data", getCounters(xizmatlar));
        model.addAttribute("names_data", getNames(xizmatlar));

        return Templates.ModelInterServiceList;
    }


    public List<String> getColors(List<InternetDtoForDashboard> sims){
        List<String> colors = new ArrayList<>();
        for (InternetDtoForDashboard sim : sims){
            colors.add(sim.getColor());
        }
        return colors;
    }
    public List<Integer> getCounters(List<InternetDtoForDashboard> sims){
        List<Integer> colors = new ArrayList<>();
        for (InternetDtoForDashboard sim : sims){
            colors.add(sim.getCount());
        }
        return colors;
    }
    public List<String> getNames(List<InternetDtoForDashboard> sims){
        List<String> colors = new ArrayList<>();
        for (InternetDtoForDashboard sim : sims){
            colors.add(sim.getOperatorName());
        }
        return colors;
    }




    @RequestMapping(value = Urls.ModelInterServiceListDataTable, method = RequestMethod.GET)
    @ResponseBody
    public DataTablesOutput<InternetXizmat> getListAsDataTables(@Valid DataTablesInput input) {
        return internetXizmatService.getDeletedTrueXizmatListInDataTableOutput(input, AuthUtils.getUserId());
    }

    @RequestMapping(value = Urls.ModelInterServiceView+"/{id}", method = RequestMethod.GET)
    public String view(@PathVariable(name = "id", required = false) Long id,
                       Model model, RedirectAttributes redirectAttributes) {
        logger.info("[view] id = " + id);
        model.addAttribute("breadcrumb", getBreadcrumbForService("action.view"));
        model.addAttribute("url_cancel", Urls.ModelInterServiceList);
        model.addAttribute("url_view", Urls.ModelInterServiceView);
        model.addAttribute("url_edit", Urls.ModelInterServiceEdit);
        model.addAttribute("url_add", Urls.ModelInterServiceCreate + "/" + id);
        logger.info("[view] model: " + model);
        return Templates.ModelInterServiceView;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelInterServiceCreate, method = RequestMethod.GET)
    public String create(Model model) {
        InternetXizmat service;
        if (!model.containsAttribute("service")){
            service = new InternetXizmat();
            logger.debug("[create] added new service: " + service);
            model.addAttribute("service", service);
        }

        List<Region> regions = regionRepository.findAll();
        model.addAttribute("regions", regions);

        List<Internet> internets = internetRepository.findAll();
        model.addAttribute("internets", internets);

        List<Operator> operators = operatorRepository.findAll();
        model.addAttribute("operators", operators);

        model.addAttribute("breadcrumb", getBreadcrumbForService("action.add"));
        model.addAttribute("url_action", Urls.ModelInterServiceSave);
        model.addAttribute("url_cancel", Urls.ModelInterServiceList);
        logger.info("[create] model: " + model);
        return Templates.ModelInterServiceEdit;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelInterServiceSave, method = RequestMethod.POST)
    public String save(InternetXizmat service, RedirectAttributes model) throws Exception{

        logger.info("[save] service = " + service);
        logger.info("[save] model = " + model);
        boolean validate = false;
        if (service.getAbonentFirstName()==null) {
            validate = true;
        }
        if (validate){
            model.addFlashAttribute("service", service);
            model.addAttribute("url_action", Urls.ModelInterServiceSave);
            model.addAttribute("url_cancel", Urls.ModelInterServiceList);
//            ToastrNotificationUtils.addError(model, messageSource.getMessage(CommonMessages.msgFailedToSaveElement, new OrganizationCategory[]{organizationCategory}, LocaleContextHolder.getLocale()));
            return "redirect:" + Urls.ModelInterServiceCreate;

        }
        User currentUser = AuthUtils.getUserEntity();
        Role role = userRoleRepository.findByUserId(currentUser.getId()).getRole();
        if (role==Role.BRANCH){
            service.setBranchId(currentUser.getBranchId());
        }
        Boolean operation = internetXizmatService.createOrUpdateByObject(service);
        if (operation == Boolean.TRUE) {
            // success save
            logger.debug("[save] service save successfully ");
//            ToastrNotificationUtils.addSuccess(model, messageSource.getMessage(CommonMessages.msgSavedSuccessfully, null, LocaleContextHolder.getLocale()));
        } else {
            // error
            logger.debug("[save] service save error ");
//            ToastrNotificationUtils.addError(model, messageSource.getMessage(CommonMessages.msgFailedToSaveElement, null, LocaleContextHolder.getLocale()));
        }

        return Properties.UrlRedirect + Urls.ModelInterServiceList;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelInterServiceEdit+"/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable(name = "id", required = false) Long id, Model model, RedirectAttributes redirectAttributes) {

        logger.info("[edit] id = " + id);
        InternetXizmat service = internetXizmatService.findById(id);
        if (service == null) {
            // not found
            logger.info("[save] requested service not found = " + service);
//            ToastrNotificationUtils.addWarning(redirectAttributes, messageSource.getMessage(CommonMessages.msgObjectNotFound, null, LocaleContextHolder.getLocale()));
            return Properties.UrlRedirect + Urls.ModelInterServiceList;
        }
        List<Region> regions = regionRepository.findAll();
        model.addAttribute("regions", regions);
        List<Internet> internets = internetRepository.findAll();
        model.addAttribute("internets", internets);

        PaketTypes[] types = PaketTypes.values();
        model.addAttribute("types", types);
        List<Operator> operators = operatorRepository.findAll();
        model.addAttribute("operators", operators);
        model.addAttribute("breadcrumb", getBreadcrumbForService("label.edit"));
        model.addAttribute("service", service);
        model.addAttribute("url_action", Urls.ModelInterServiceSave);
        model.addAttribute("url_cancel", Urls.ModelInterServiceList);
        logger.info("[edit] model: " + model);
        return Templates.ModelInterServiceEdit;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelInterServiceDelete)
    public String delete(@RequestParam(name = "id", required = true) Long id,
                         RedirectAttributes redirectAttributes, Model model) {
        logger.info("[delete] id = " + id);
        InternetXizmat service = internetXizmatService.findById(id);
        if (service == null) {
            // not found
            logger.info("[delete] requested service not found: " + service);
//            ToastrNotificationUtils.addWarning(redirectAttributes, messageSource.getMessage(CommonMessages.msgObjectNotFound, null, LocaleContextHolder.getLocale()));
            return Properties.UrlRedirect + Urls.ModelInterServiceList;
        }
        internetXizmatService.delete(service);
        logger.debug("[delete] service deleted: " + service);
//        ToastrNotificationUtils.addSuccess(redirectAttributes, messageSource.getMessage(CommonMessages.msgDeletedSuccessfully, null, LocaleContextHolder.getLocale()));
        return Properties.UrlRedirect + Urls.ModelInterServiceList;
    }

    public Breadcrumb getBreadcrumbForService(String name) {
        Breadcrumb breadcrumb = new Breadcrumb();
        breadcrumb.addLink("service", Urls.ModelInterServiceList);
        return breadcrumb;
    }
}