package com.datamond.crm.controller;


import com.datamond.crm.constant.*;
import com.datamond.crm.dto.dashboard.SimcardDtoForDashboard;
import com.datamond.crm.dto.dashboard.SmsDtoForDashboard;
import com.datamond.crm.entity.*;
import com.datamond.crm.repository.*;
import com.datamond.crm.service.SmsXizmatService;
import com.datamond.crm.utils.AuthUtils;
import com.datamond.crm.utils.Breadcrumb;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class SmsXizmatController {

    private Logger logger = LogManager.getLogger(SmsXizmatController.class);

    private SmsXizmatService xizmatService;

    @Autowired
    private OperatorRepository operatorRepository;

    @Autowired
    private RegionRepository regionRepository;

    @Autowired
    private SmsRepository smsRepository;

    @Autowired
    private InternetRepository internetRepository ;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    public SmsXizmatController(SmsXizmatService xizmatService) {
        this.xizmatService = xizmatService;
    }

    @RequestMapping(value = {Urls.ModelServiceList})
    public String getXizmatList(Model model) {
        logger.info("[employee control] model: " + model);
        model.addAttribute("breadcrumb", getBreadcrumbForService("label.list"));
        model.addAttribute("url_edit", Urls.ModelServiceEdit);
        model.addAttribute("url_view", Urls.ModelServiceView);
        model.addAttribute("url_ajax", Urls.ModelServiceListDataTable);
        model.addAttribute("url_add", Urls.ModelServiceCreate);
        model.addAttribute("url_delete", Urls.ModelServiceDelete);
        logger.info("[organization control] model: " + model);

        List<SmsDtoForDashboard> xizmats = xizmatService.getData(AuthUtils.getUserId(), AuthUtils.getUserEntity().getBranchId());
        model.addAttribute("names", getNames(xizmats));
        model.addAttribute("counts", getCounters(xizmats));
        model.addAttribute("colors", getColors(xizmats));


        List<SmsDtoForDashboard> xizmatlar = xizmatService.getDataForComing(AuthUtils.getUserId(), AuthUtils.getUserEntity().getBranchId());
        model.addAttribute("date", "Oxirgi 30 kun");
        model.addAttribute("counter_data", getCounters(xizmatlar));
        model.addAttribute("names_data", getNames(xizmatlar));

        return Templates.ModelServiceList;
    }


    public List<String> getColors(List<SmsDtoForDashboard> sims){
        List<String> colors = new ArrayList<>();
        for (SmsDtoForDashboard sim : sims){
            colors.add(sim.getColor());
        }
        return colors;
    }
    public List<Integer> getCounters(List<SmsDtoForDashboard> sims){
        List<Integer> colors = new ArrayList<>();
        for (SmsDtoForDashboard sim : sims){
            colors.add(sim.getCount());
        }
        return colors;
    }
    public List<String> getNames(List<SmsDtoForDashboard> sims){
        List<String> colors = new ArrayList<>();
        for (SmsDtoForDashboard sim : sims){
            colors.add(sim.getOperatorName());
        }
        return colors;
    }


    @RequestMapping(value = Urls.ModelServiceListDataTable, method = RequestMethod.GET)
    @ResponseBody
    public DataTablesOutput<SmsXizmat> getListAsDataTables(@Valid DataTablesInput input) {
        return xizmatService.getDeletedTrueServiceListInDataTableOutput(input, AuthUtils.getUserId());
    }

    @RequestMapping(value = Urls.ModelServiceView+"/{id}", method = RequestMethod.GET)
    public String view(@PathVariable(name = "id", required = false) Long id,
                       Model model, RedirectAttributes redirectAttributes) {
        logger.info("[view] id = " + id);
        model.addAttribute("breadcrumb", getBreadcrumbForService("action.view"));
        model.addAttribute("url_cancel", Urls.ModelServiceList);
        model.addAttribute("url_view", Urls.ModelServiceView);
        model.addAttribute("url_edit", Urls.ModelServiceEdit);
        model.addAttribute("url_add", Urls.ModelServiceCreate + "/" + id);
        logger.info("[view] model: " + model);
        return Templates.ModelServiceView;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelServiceCreate, method = RequestMethod.GET)
    public String create(Model model) {
        SmsXizmat service;
        if (!model.containsAttribute("service")){
            service = new SmsXizmat();
            logger.debug("[create] added new service: " + service);
            model.addAttribute("service", service);
        }

        List<Region> regions = regionRepository.findAll();
        model.addAttribute("regions", regions);

        PaketTypes[] types = PaketTypes.values();
        model.addAttribute("types", types);

        List<Sms> smss = smsRepository.findAll();
        model.addAttribute("smss", smss);

        List<Operator> operators = operatorRepository.findAll();
        model.addAttribute("operators", operators);

        model.addAttribute("breadcrumb", getBreadcrumbForService("action.add"));
        model.addAttribute("url_action", Urls.ModelServiceSave);
        model.addAttribute("url_cancel", Urls.ModelServiceList);
        logger.info("[create] model: " + model);
        return Templates.ModelServiceEdit;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelServiceSave, method = RequestMethod.POST)
    public String save(SmsXizmat service, RedirectAttributes model) throws Exception{

        logger.info("[save] service = " + service);
        logger.info("[save] model = " + model);
        boolean validate = false;
        if (service.getAbonentFirstName()==null) {
            validate = true;
        }
        if (validate){
            model.addFlashAttribute("service", service);
            model.addAttribute("url_action", Urls.ModelServiceSave);
            model.addAttribute("url_cancel", Urls.ModelServiceList);
//            ToastrNotificationUtils.addError(model, messageSource.getMessage(CommonMessages.msgFailedToSaveElement, new OrganizationCategory[]{organizationCategory}, LocaleContextHolder.getLocale()));
            return "redirect:" + Urls.ModelServiceCreate;

        }
        User currentUser = AuthUtils.getUserEntity();
        Role role = userRoleRepository.findByUserId(currentUser.getId()).getRole();
        if (role==Role.BRANCH){
            service.setBranchId(currentUser.getBranchId());
        }
        Boolean operation = xizmatService.createOrUpdateByObject(service);
        if (operation == Boolean.TRUE) {
            // success save
            logger.debug("[save] service save successfully ");
//            ToastrNotificationUtils.addSuccess(model, messageSource.getMessage(CommonMessages.msgSavedSuccessfully, null, LocaleContextHolder.getLocale()));
        } else {
            // error
            logger.debug("[save] service save error ");
//            ToastrNotificationUtils.addError(model, messageSource.getMessage(CommonMessages.msgFailedToSaveElement, null, LocaleContextHolder.getLocale()));
        }

        return Properties.UrlRedirect + Urls.ModelServiceList;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelServiceEdit+"/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable(name = "id", required = false) Long id, Model model, RedirectAttributes redirectAttributes) {

        logger.info("[edit] id = " + id);
        SmsXizmat service = xizmatService.findById(id);
        if (service == null) {
            // not found
            logger.info("[save] requested service not found = " + service);
//            ToastrNotificationUtils.addWarning(redirectAttributes, messageSource.getMessage(CommonMessages.msgObjectNotFound, null, LocaleContextHolder.getLocale()));
            return Properties.UrlRedirect + Urls.ModelServiceList;
        }
        List<Region> regions = regionRepository.findAll();
        model.addAttribute("regions", regions);
        List<Sms> smss = smsRepository.findAll();
        model.addAttribute("smss", smss);

        PaketTypes[] types = PaketTypes.values();
        model.addAttribute("types", types);
        List<Operator> operators = operatorRepository.findAll();
        model.addAttribute("operators", operators);
        model.addAttribute("breadcrumb", getBreadcrumbForService("label.edit"));
        model.addAttribute("service", service);
        model.addAttribute("url_action", Urls.ModelServiceSave);
        model.addAttribute("url_cancel", Urls.ModelServiceList);
        logger.info("[edit] model: " + model);
        return Templates.ModelServiceEdit;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelServiceDelete)
    public String delete(@RequestParam(name = "id", required = true) Long id,
                         RedirectAttributes redirectAttributes, Model model) {
        logger.info("[delete] id = " + id);
        SmsXizmat service = xizmatService.findById(id);
        if (service == null) {
            // not found
            logger.info("[delete] requested service not found: " + service);
//            ToastrNotificationUtils.addWarning(redirectAttributes, messageSource.getMessage(CommonMessages.msgObjectNotFound, null, LocaleContextHolder.getLocale()));
            return Properties.UrlRedirect + Urls.ModelServiceList;
        }
        xizmatService.delete(service);
        logger.debug("[delete] service deleted: " + service);
//        ToastrNotificationUtils.addSuccess(redirectAttributes, messageSource.getMessage(CommonMessages.msgDeletedSuccessfully, null, LocaleContextHolder.getLocale()));
        return Properties.UrlRedirect + Urls.ModelServiceList;
    }

    public Breadcrumb getBreadcrumbForService(String name) {
        Breadcrumb breadcrumb = new Breadcrumb();
        breadcrumb.addLink("service", Urls.ModelServiceList);
        return breadcrumb;
    }
}