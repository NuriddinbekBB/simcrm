package com.datamond.crm.controller;


import com.datamond.crm.entity.Item;
import com.datamond.crm.entity.UnitType;
import com.datamond.crm.repository.ItemRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class ItemController {

    @Autowired
    private ItemRepo itemRepo;



    @GetMapping("/items")
    public String listCategory(Model model){
        List<Item> listCategories = itemRepo.findAll();
        model.addAttribute("listitem",listCategories);
        return "items";
    }

    @GetMapping("/items/new")
    public String createCategory(Model model){
        model.addAttribute("item",new Item());
        model.addAttribute("unitTypes", UnitType.values());
      return "item_form";
    }

    @PostMapping("/item/save")
    public String saveCateegory(Item item){
        itemRepo.save(item);

        return "redirect:/items";
    }

    @GetMapping("item/edit/{id}")
    public String editCategory(@PathVariable("id") Integer id, Model model){
        Item item = itemRepo.findById(id).get();
        model.addAttribute("category", item);
        return "item_form";
    }

    @GetMapping("item/delete/{id}")
    public String deleteCategory(@PathVariable("id") Integer id, Model model){
        itemRepo.deleteById(id);
        return "redirect:/items";
    }


}
