package com.datamond.crm.controller;

import com.datamond.crm.constant.Urls;
import com.datamond.crm.utils.Breadcrumb;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class ExceptionController {
    private static Logger logger = LogManager.getLogger(ExceptionController.class);

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoHandlerFoundException.class)
    public String handleError404(HttpServletRequest request, Exception e, Model model) {
        logger.error("Request: " + request.getRequestURL(), e);
        model.addAttribute("breadcrumb", getBreadcrumbForException("label.404"));
        return "404";
    }

    @ResponseStatus(HttpStatus.NETWORK_AUTHENTICATION_REQUIRED)
    @ExceptionHandler(AccessDeniedException.class)
    public String handleAccessDeniedException(AccessDeniedException e, HttpServletRequest request, Model model) {
        logger.error("Request: " + request.getRequestURL(), e);
        model.addAttribute("breadcrumb", getBreadcrumbForException("label.403"));
        return "403";
    }

    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String handleError(HttpServletRequest request, Exception e, Model model) {
        logger.error("Request: " + request.getRequestURL(), e);
        model.addAttribute("breadcrumb", getBreadcrumbForException("label.500"));
        return "500";
    }
    public Breadcrumb getBreadcrumbForException(String name) {
        Breadcrumb breadcrumb = new Breadcrumb();
        breadcrumb.addLink("menu.dashboard", Urls.AdminDashboard);
//        breadcrumb.addLink(name, "#");
        return breadcrumb;
    }
}