package com.datamond.crm.controller;


import com.datamond.crm.constant.Properties;
import com.datamond.crm.constant.Templates;
import com.datamond.crm.constant.Urls;
import com.datamond.crm.entity.Branch;
import com.datamond.crm.entity.Region;
import com.datamond.crm.repository.OperatorRepository;
import com.datamond.crm.repository.RegionRepository;
import com.datamond.crm.service.BranchService;
import com.datamond.crm.service.RegionService;
import com.datamond.crm.utils.Breadcrumb;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

@Controller
public class RegionController {

    private Logger logger = LogManager.getLogger(RegionController.class);

    private RegionService regionService;

    @Autowired
    private OperatorRepository operatorRepository;

    @Autowired
    private RegionRepository regionRepository;

    @Autowired
    public RegionController(RegionService regionService) {
        this.regionService = regionService;
    }

    @RequestMapping(value = {Urls.ModelRegionList})
    public String getRegionList(Model model) {
        logger.info("[branch control] model: " + model);
        model.addAttribute("breadcrumb", getBreadcrumbForRegion("label.list"));
        model.addAttribute("url_edit", Urls.ModelRegionEdit);
        model.addAttribute("url_view", Urls.ModelRegionView);
        model.addAttribute("url_ajax", Urls.ModelRegionListDataTable);
        model.addAttribute("url_add", Urls.ModelRegionCreate);
        model.addAttribute("url_delete", Urls.ModelRegionDelete);
        logger.info("[organization control] model: " + model);
        return Templates.ModelRegionList;
    }


    @RequestMapping(value = Urls.ModelRegionListDataTable, method = RequestMethod.GET)
    @ResponseBody
    public DataTablesOutput<Region> getListAsDataTables(@Valid DataTablesInput input) {
        return regionService.getDeletedTrueRegionListInDataTableOutput(input);
    }

    @RequestMapping(value = Urls.ModelRegionView+"/{id}", method = RequestMethod.GET)
    public String view(@PathVariable(name = "id", required = false) Long id,
                       Model model, RedirectAttributes redirectAttributes) {
        logger.info("[view] id = " + id);
        model.addAttribute("breadcrumb", getBreadcrumbForRegion("action.view"));
        model.addAttribute("url_cancel", Urls.ModelRegionList);
        model.addAttribute("url_view", Urls.ModelRegionView);
        model.addAttribute("url_edit", Urls.ModelRegionEdit);
        model.addAttribute("url_add", Urls.ModelRegionCreate + "/" + id);
//        model.addAttribute("employee", employee);
        logger.info("[view] model: " + model);
        return Templates.ModelRegionView;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelRegionCreate, method = RequestMethod.GET)
    public String create(Model model) {
        Region region;
        if (!model.containsAttribute("region")){
            region = new Region();
            logger.debug("[create] added new region: " + region);
            model.addAttribute("region", region);
        }
        model.addAttribute("breadcrumb", getBreadcrumbForRegion("action.add"));
        model.addAttribute("url_action", Urls.ModelRegionSave);
        model.addAttribute("url_cancel", Urls.ModelRegionList);
        logger.info("[create] model: " + model);
        return Templates.ModelRegionEdit;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelRegionSave, method = RequestMethod.POST)
    public String save(Region region, RedirectAttributes model) {

        logger.info("[save] region = " + region);
        logger.info("[save] model = " + model);
        boolean validate = false;
        if (region.getRegionName()==null) {
            validate = true;
        }
        if (validate){
            model.addFlashAttribute("region", region);
            model.addAttribute("url_action", Urls.ModelRegionSave);
            model.addAttribute("url_cancel", Urls.ModelRegionList);
//            ToastrNotificationUtils.addError(model, messageSource.getMessage(CommonMessages.msgFailedToSaveElement, new OrganizationCategory[]{organizationCategory}, LocaleContextHolder.getLocale()));
            return "redirect:" + Urls.ModelRegionCreate;

        }

        Boolean operation = regionService.createOrUpdateByObject(region);
        if (operation == Boolean.TRUE) {
            // success save
            logger.debug("[save] region save successfully ");
//            ToastrNotificationUtils.addSuccess(model, messageSource.getMessage(CommonMessages.msgSavedSuccessfully, null, LocaleContextHolder.getLocale()));
        } else {
            // error
            logger.debug("[save] region save error ");
//            ToastrNotificationUtils.addError(model, messageSource.getMessage(CommonMessages.msgFailedToSaveElement, null, LocaleContextHolder.getLocale()));
        }

        return Properties.UrlRedirect + Urls.ModelRegionList;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelRegionEdit+"/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable(name = "id", required = false) Long id, Model model, RedirectAttributes redirectAttributes) {

        logger.info("[edit] id = " + id);
        Region region = regionService.findById(id);
        if (region == null) {
            // not found
            logger.info("[save] requested region not found = " + region);
//            ToastrNotificationUtils.addWarning(redirectAttributes, messageSource.getMessage(CommonMessages.msgObjectNotFound, null, LocaleContextHolder.getLocale()));
            return Properties.UrlRedirect + Urls.ModelRegionList;
        }

        model.addAttribute("breadcrumb", getBreadcrumbForRegion("label.edit"));
        model.addAttribute("region", region);
        model.addAttribute("url_action", Urls.ModelRegionSave);
        model.addAttribute("url_cancel", Urls.ModelRegionList);
        logger.info("[edit] model: " + model);
        return Templates.ModelRegionEdit;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelRegionDelete)
    public String delete(@RequestParam(name = "id", required = true) Long id,
                         RedirectAttributes redirectAttributes, Model model) {
        logger.info("[delete] id = " + id);
        Region region = regionService.findById(id);
        if (region == null) {
            // not found
            logger.info("[delete] requested region not found: " + region);
//            ToastrNotificationUtils.addWarning(redirectAttributes, messageSource.getMessage(CommonMessages.msgObjectNotFound, null, LocaleContextHolder.getLocale()));
            return Properties.UrlRedirect + Urls.ModelRegionList;
        }
        regionService.delete(region);
        logger.debug("[delete] region deleted: " + region);
//        ToastrNotificationUtils.addSuccess(redirectAttributes, messageSource.getMessage(CommonMessages.msgDeletedSuccessfully, null, LocaleContextHolder.getLocale()));
        return Properties.UrlRedirect + Urls.ModelRegionList;
    }

    public Breadcrumb getBreadcrumbForRegion(String name) {
        Breadcrumb breadcrumb = new Breadcrumb();
        breadcrumb.addLink("region", Urls.ModelRegionList);
        return breadcrumb;
    }



}