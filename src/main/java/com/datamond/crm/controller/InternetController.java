package com.datamond.crm.controller;


import com.datamond.crm.constant.PaketTypes;
import com.datamond.crm.constant.Properties;
import com.datamond.crm.constant.Templates;
import com.datamond.crm.constant.Urls;
import com.datamond.crm.entity.Internet;
import com.datamond.crm.entity.Operator;
import com.datamond.crm.entity.Sms;
import com.datamond.crm.repository.OperatorRepository;
import com.datamond.crm.repository.SmsRepository;
import com.datamond.crm.service.InternetService;
import com.datamond.crm.service.SmsService;
import com.datamond.crm.utils.Breadcrumb;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.relational.core.sql.In;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

@Controller
public class InternetController {

    private Logger logger = LogManager.getLogger(InternetController.class);

    private InternetService internetService;

    @Autowired
    private OperatorRepository operatorRepository;

    @Autowired
    private SmsRepository smsRepository;

    @Autowired
    public InternetController(InternetService internetService) {
        this.internetService = internetService;
    }

    @RequestMapping(value = {Urls.ModelInternetList})
    public String getInternetList(Model model) {
        logger.info("[branch control] model: " + model);
        model.addAttribute("breadcrumb", getBreadcrumbForPaket("label.list"));
        model.addAttribute("url_edit", Urls.ModelInternetEdit);
        model.addAttribute("url_view", Urls.ModelInternetView);
        model.addAttribute("url_ajax", Urls.ModelInternetListDataTable);
        model.addAttribute("url_add", Urls.ModelInternetCreate);
        model.addAttribute("url_delete", Urls.ModelInternetDelete);
        logger.info("[organization control] model: " + model);
        return Templates.ModelInternetList;
    }


    @RequestMapping(value = Urls.ModelInternetListDataTable, method = RequestMethod.GET)
    @ResponseBody
    public DataTablesOutput<Internet> getListAsDataTables(@Valid DataTablesInput input) {
        return internetService.getDeletedTrueInternetListInDataTableOutput(input);
    }

    @RequestMapping(value = Urls.ModelInternetView+"/{id}", method = RequestMethod.GET)
    public String view(@PathVariable(name = "id", required = false) Long id,
                       Model model, RedirectAttributes redirectAttributes) {
        logger.info("[view] id = " + id);
        model.addAttribute("breadcrumb", getBreadcrumbForPaket("action.view"));
        model.addAttribute("url_cancel", Urls.ModelInternetList);
        model.addAttribute("url_view", Urls.ModelInternetView);
        model.addAttribute("url_edit", Urls.ModelInternetEdit);
        model.addAttribute("url_add", Urls.ModelInternetCreate + "/" + id);
        logger.info("[view] model: " + model);
        return Templates.ModelInternetView;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelInternetCreate, method = RequestMethod.GET)
    public String create(Model model) {
        Internet internet;
        if (!model.containsAttribute("internet")){
            internet = new Internet();
            logger.debug("[create] added new internet: " + internet);
            model.addAttribute("internet", internet);
        }
        List<Operator> operators = operatorRepository.findAll();
        model.addAttribute("opers", operators);
        PaketTypes[] types = PaketTypes.values();
        model.addAttribute("types", types);
        model.addAttribute("breadcrumb", getBreadcrumbForPaket("action.add"));
        model.addAttribute("url_action", Urls.ModelInternetSave);
        model.addAttribute("url_cancel", Urls.ModelInternetList);
        logger.info("[create] model: " + model);
        return Templates.ModelInternetEdit;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelInternetSave, method = RequestMethod.POST)
    public String save(Internet internet, RedirectAttributes model) {

        logger.info("[save] internet = " + internet);
        logger.info("[save] model = " + model);
        boolean validate = false;
        if (internet.getPaketName()==null) {
            validate = true;
        }
        if (validate){
            model.addFlashAttribute("internet", internet);
            model.addAttribute("url_action", Urls.ModelInternetSave);
            model.addAttribute("url_cancel", Urls.ModelInternetList);
//            ToastrNotificationUtils.addError(model, messageSource.getMessage(CommonMessages.msgFailedToSaveElement, new OrganizationCategory[]{organizationCategory}, LocaleContextHolder.getLocale()));
            return "redirect:" + Urls.ModelInternetCreate;

        }

        Boolean operation = internetService.createOrUpdateByObject(internet);
        if (operation == Boolean.TRUE) {
            // success save
            logger.debug("[save] internet save successfully ");
//            ToastrNotificationUtils.addSuccess(model, messageSource.getMessage(CommonMessages.msgSavedSuccessfully, null, LocaleContextHolder.getLocale()));
        } else {
            // error
            logger.debug("[save] internet save error ");
//            ToastrNotificationUtils.addError(model, messageSource.getMessage(CommonMessages.msgFailedToSaveElement, null, LocaleContextHolder.getLocale()));
        }

        return Properties.UrlRedirect + Urls.ModelInternetList;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelInternetEdit+"/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable(name = "id", required = false) Long id, Model model, RedirectAttributes redirectAttributes) {

        logger.info("[edit] id = " + id);
        Internet internet = internetService.findById(id);
        if (internet == null) {
            // not found
            logger.info("[save] requested internet not found = " + internet);
//            ToastrNotificationUtils.addWarning(redirectAttributes, messageSource.getMessage(CommonMessages.msgObjectNotFound, null, LocaleContextHolder.getLocale()));
            return Properties.UrlRedirect + Urls.ModelInternetList;
        }
        List<Operator> operators = operatorRepository.findAll();
        model.addAttribute("opers", operators);
        PaketTypes[] types = PaketTypes.values();
        model.addAttribute("types", types);
        model.addAttribute("breadcrumb", getBreadcrumbForPaket("label.edit"));
        model.addAttribute("internet", internet);
        model.addAttribute("url_action", Urls.ModelInternetSave);
        model.addAttribute("url_cancel", Urls.ModelInternetList);
        logger.info("[edit] model: " + model);
        return Templates.ModelInternetEdit;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelInternetDelete)
    public String delete(@RequestParam(name = "id", required = true) Long id,
                         RedirectAttributes redirectAttributes, Model model) {
        logger.info("[delete] id = " + id);
        Internet internet = internetService.findById(id);
        if (internet == null) {
            // not found
            logger.info("[delete] requested internet not found: " + internet);
//            ToastrNotificationUtils.addWarning(redirectAttributes, messageSource.getMessage(CommonMessages.msgObjectNotFound, null, LocaleContextHolder.getLocale()));
            return Properties.UrlRedirect + Urls.ModelInternetList;
        }
        internetService.delete(internet);
        logger.debug("[delete] internet deleted: " + internet);
//        ToastrNotificationUtils.addSuccess(redirectAttributes, messageSource.getMessage(CommonMessages.msgDeletedSuccessfully, null, LocaleContextHolder.getLocale()));
        return Properties.UrlRedirect + Urls.ModelInternetList;
    }

    public Breadcrumb getBreadcrumbForPaket(String name) {
        Breadcrumb breadcrumb = new Breadcrumb();
        breadcrumb.addLink("internet", Urls.ModelInternetList);
        return breadcrumb;
    }



}