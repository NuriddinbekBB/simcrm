package com.datamond.crm.controller;


import com.datamond.crm.constant.Properties;
import com.datamond.crm.constant.Templates;
import com.datamond.crm.constant.Urls;
import com.datamond.crm.entity.Branch;
import com.datamond.crm.entity.Operator;
import com.datamond.crm.entity.Region;
import com.datamond.crm.entity.Simcard;
import com.datamond.crm.repository.OperatorRepository;
import com.datamond.crm.repository.RegionRepository;
import com.datamond.crm.service.BranchService;
import com.datamond.crm.service.SimcardService;
import com.datamond.crm.utils.AuthUtils;
import com.datamond.crm.utils.Breadcrumb;
import com.datamond.crm.utils.DateParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Controller
public class BranchController {

    private Logger logger = LogManager.getLogger(BranchController.class);

    private BranchService branchService;

    @Autowired
    private OperatorRepository operatorRepository;

    @Autowired
    private RegionRepository regionRepository;

    @Autowired
    public BranchController(BranchService branchService) {
        this.branchService = branchService;
    }

    @RequestMapping(value = {Urls.ModelBranchList})
    public String getBranchList(Model model) {
        logger.info("[branch control] model: " + model);
        model.addAttribute("breadcrumb", getBreadcrumbForBranch("label.list"));
        model.addAttribute("url_edit", Urls.ModelBranchEdit);
        model.addAttribute("url_view", Urls.ModelBranchView);
        model.addAttribute("url_ajax", Urls.ModelBranchListDataTable);
        model.addAttribute("url_view_modal", Urls.ModelBranchAddModal);
        model.addAttribute("url_add", Urls.ModelBranchCreate);
        model.addAttribute("url_delete", Urls.ModelBranchDelete);
        logger.info("[organization control] model: " + model);
        return Templates.ModelBranchList;
    }


    @RequestMapping(value = Urls.ModelBranchListDataTable, method = RequestMethod.GET)
    @ResponseBody
    public DataTablesOutput<Branch> getListAsDataTables(@Valid DataTablesInput input) {
        return branchService.getDeletedTrueBranchListInDataTableOutput(input);
    }

    @RequestMapping(value = Urls.ModelBranchView+"/{id}", method = RequestMethod.GET)
    public String view(@PathVariable(name = "id", required = false) Long id,
                       Model model, RedirectAttributes redirectAttributes) {
        logger.info("[view] id = " + id);
        model.addAttribute("breadcrumb", getBreadcrumbForBranch("action.view"));
        model.addAttribute("url_cancel", Urls.ModelBranchList);
        model.addAttribute("url_view", Urls.ModelBranchView);
        model.addAttribute("url_edit", Urls.ModelBranchEdit);
        model.addAttribute("url_add", Urls.ModelBranchCreate + "/" + id);
//        model.addAttribute("employee", employee);
        logger.info("[view] model: " + model);
        return Templates.ModelBranchView;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelBranchCreate, method = RequestMethod.GET)
    public String create(Model model) {
        Branch branch;
//        Position position = positionService.findById(id);
        if (!model.containsAttribute("simcard")){
            branch = new Branch();
            logger.debug("[create] added new branch: " + branch);
            model.addAttribute("branch", branch);
        }
        List<Region> regions = regionRepository.findAll();
        model.addAttribute("regions", regions);
        model.addAttribute("breadcrumb", getBreadcrumbForBranch("action.add"));
        model.addAttribute("url_action", Urls.ModelBranchSave);
        model.addAttribute("url_cancel", Urls.ModelBranchList);
        logger.info("[create] model: " + model);
        return Templates.ModelBranchEdit;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelBranchSave, method = RequestMethod.POST)
    public String save(Branch branch, RedirectAttributes model) {

        logger.info("[save] branch = " + branch);
        logger.info("[save] model = " + model);
        boolean validate = false;
        if (branch.getBranchName()==null) {
            validate = true;
        }
        if (validate){
            model.addFlashAttribute("branch", branch);
            model.addAttribute("url_action", Urls.ModelBranchSave);
            model.addAttribute("url_cancel", Urls.ModelBranchList);
//            ToastrNotificationUtils.addError(model, messageSource.getMessage(CommonMessages.msgFailedToSaveElement, new OrganizationCategory[]{organizationCategory}, LocaleContextHolder.getLocale()));
            return "redirect:" + Urls.ModelBranchCreate;

        }

        Boolean operation = branchService.createOrUpdateByObject(branch);
        if (operation == Boolean.TRUE) {
            // success save
            logger.debug("[save] branch save successfully ");
//            ToastrNotificationUtils.addSuccess(model, messageSource.getMessage(CommonMessages.msgSavedSuccessfully, null, LocaleContextHolder.getLocale()));
        } else {
            // error
            logger.debug("[save] branch save error ");
//            ToastrNotificationUtils.addError(model, messageSource.getMessage(CommonMessages.msgFailedToSaveElement, null, LocaleContextHolder.getLocale()));
        }

        return Properties.UrlRedirect + Urls.ModelBranchList;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelBranchEdit+"/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable(name = "id", required = false) Long id, Model model, RedirectAttributes redirectAttributes) {

        logger.info("[edit] id = " + id);
        Branch branch = branchService.findById(id);
        if (branch == null) {
            // not found
            logger.info("[save] requested branch not found = " + branch);
//            ToastrNotificationUtils.addWarning(redirectAttributes, messageSource.getMessage(CommonMessages.msgObjectNotFound, null, LocaleContextHolder.getLocale()));
            return Properties.UrlRedirect + Urls.ModelBranchList;
        }

        List<Region> regions = regionRepository.findAll();
        model.addAttribute("regions", regions);
        model.addAttribute("breadcrumb", getBreadcrumbForBranch("label.edit"));
        model.addAttribute("branch", branch);
        model.addAttribute("url_action", Urls.ModelBranchSave);
        model.addAttribute("url_cancel", Urls.ModelBranchList);
        logger.info("[edit] model: " + model);
        return Templates.ModelBranchEdit;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelBranchDelete)
    public String delete(@RequestParam(name = "id", required = true) Long id,
                         RedirectAttributes redirectAttributes, Model model) {
        logger.info("[delete] id = " + id);
        Branch branch = branchService.findById(id);
        if (branch == null) {
            // not found
            logger.info("[delete] requested branch not found: " + branch);
//            ToastrNotificationUtils.addWarning(redirectAttributes, messageSource.getMessage(CommonMessages.msgObjectNotFound, null, LocaleContextHolder.getLocale()));
            return Properties.UrlRedirect + Urls.ModelBranchList;
        }
        branchService.delete(branch);
        logger.debug("[delete] branch deleted: " + branch);
//        ToastrNotificationUtils.addSuccess(redirectAttributes, messageSource.getMessage(CommonMessages.msgDeletedSuccessfully, null, LocaleContextHolder.getLocale()));
        return Properties.UrlRedirect + Urls.ModelBranchList;
    }


    public Breadcrumb getBreadcrumbForBranch(String name) {
        Breadcrumb breadcrumb = new Breadcrumb();
        breadcrumb.addLink("branch", Urls.ModelBranchList);
        return breadcrumb;
    }



}