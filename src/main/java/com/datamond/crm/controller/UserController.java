package com.datamond.crm.controller;

import com.datamond.crm.constant.Properties;
import com.datamond.crm.constant.Role;
import com.datamond.crm.constant.Templates;
import com.datamond.crm.constant.Urls;
import com.datamond.crm.entity.Branch;
import com.datamond.crm.entity.User;
import com.datamond.crm.entity.UserRole;
import com.datamond.crm.repository.UserRoleRepository;
import com.datamond.crm.service.BranchService;
import com.datamond.crm.service.UserRoleService;
import com.datamond.crm.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

@Controller
public class UserController {

    private Logger logger = LogManager.getLogger(UserController.class);

    private final UserService userService;
    private final UserRoleRepository userRoleRepository;
    private MessageSource messageSource;
    private UserRoleService userRoleService;
    private BranchService branchService;

    @Autowired
    public UserController(UserRoleService userRoleService, BranchService branchService, UserService userService, UserRoleRepository userRoleRepository) {
        this.userService = userService;
        this.branchService = branchService;
        this.userRoleRepository = userRoleRepository;
        this.userRoleService = userRoleService;
    }

//    @PreAuthorize("hasAuthority('ADMINISTRATOR')")
    @RequestMapping(value = Urls.AdminUsersList)
    public String getUserList(Model model) {
        logger.debug(" user list ");

        model.addAttribute("url_edit", Urls.AdminUsersEdit);
        model.addAttribute("url_view", Urls.AdminUsersView);
        model.addAttribute("url_ajax", Urls.AdminUsersListDataTable);
        model.addAttribute("url_add", Urls.AdminUsersCreate);
        model.addAttribute("url_delete", Urls.AdminUsersDelete);
        logger.info("[getUserList] model : " + model);

        return Templates.AdminUsersList;
    }

    @RequestMapping(value = Urls.AdminUsersListDataTable, method = RequestMethod.GET)
    @ResponseBody
    public DataTablesOutput<User> getListAsDataTables(@Valid DataTablesInput input) {
        return userService.getDeletedTrueUserListInDataTableOutput(input);
    }


//    @PreAuthorize("hasAuthority('ADMINISTRATOR') or hasAuthority('MANAGER') or hasAuthority('ORGANIZATION_ADMINISTRATOR') or hasAuthority('MERCHANT') or hasAuthority('CALL_CENTER')")
    @RequestMapping(value = Urls.AdminUsersCreate, method = RequestMethod.GET)
    public String create(Model model, Authentication authentication) {
        Role r = Role.valueOf(authentication.getAuthorities().toArray()[0].toString());
//        Role r = Role.valueOf("ADMINISTRATOR");
        model.addAttribute("roles", r.allChildren());
        logger.info("[create] roles of user : " + Arrays.toString(r.allChildren()));
        User user;
        List<Branch> branches = branchService.findAll();
        model.addAttribute("branches", branches);
        if (!model.containsAttribute("user")) {

//            model.addAttribute("breadcrumb", getBreadcrumbForUsers("action.add"));
            user = new User();
            logger.info("[create] create new user : " + user);
            model.addAttribute("user", user);
        }
        model.addAttribute("url_action", Urls.AdminUsersSave);
        model.addAttribute("url_cancel", Urls.AdminUsersList);
        logger.info("[create] model : " + model);
        return Templates.AdminUsersEdit;
    }

//    @PreAuthorize("hasAuthority('ADMINISTRATOR') or hasAuthority('MANAGER') or hasAuthority('ORGANIZATION_ADMINISTRATOR') or hasAuthority('MERCHANT') or hasAuthority('CALL_CENTER')")
    @RequestMapping(value = Urls.AdminUsersEdit + "/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable(name = "id") Long id, Model model, Authentication authentication) {
        User user = userService.findById(id);
        logger.info("[edit] edit user = " + user);
        if (user == null) {
//            ToastrNotificationUtils.addError(
//                    (RedirectAttributes) model,
//                    messageSource.getMessage(CommonMessages.msgObjectNotFound, null, LocaleContextHolder.getLocale()));
            return Properties.UrlRedirect + Urls.AdminUsersList;
        }
        Role r = Role.valueOf(authentication.getAuthorities().toArray()[0].toString());
        List<Branch> branches = branchService.findAll();
        model.addAttribute("branches", branches);
        model.addAttribute("roles", r.allChildren());
        UserRole userRole = userRoleRepository.findByUserId(id);
        logger.debug("[edit] user set role: " + userRole.getRole().toString());
        user.setRole(userRole.getRole().toString());
        model.addAttribute("user", user);
        model.addAttribute("url_action", Urls.AdminUsersSave);
        model.addAttribute("url_cancel", Urls.AdminUsersList);
        logger.info("[edit] model : " + model);
        return Templates.AdminUsersEdit;
    }

//    @PreAuthorize("hasAuthority('ADMINISTRATOR') or hasAuthority('MANAGER') or hasAuthority('ORGANIZATION_ADMINISTRATOR') or hasAuthority('MERCHANT') or hasAuthority('CALL_CENTER')")
    @RequestMapping(value = Urls.AdminUsersSave, method = RequestMethod.POST)
    public String save(
            User user,
            @RequestParam("password") String password1,
            @RequestParam("password-confirm") String repeatPassword,
            RedirectAttributes model
    ) {
        logger.info("[save] save user with : password1 = " + password1 + " repeatPassword = " + repeatPassword + " user: " + user);

        if (!password1.isEmpty() && !repeatPassword.isEmpty() && password1.equals(repeatPassword)) {
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            logger.debug("[save] set user password : " + passwordEncoder.encode(password1));
            user.setPassword(passwordEncoder.encode(password1));
        } else {
            return null;
        }

        boolean validate = false;
        if (user.getUsername().isEmpty()) {
            validate = true;
        }
        if (user.getFirstname().isEmpty()) {
            validate = true;
        }
        if (user.getLastname().isEmpty()) {
            validate = true;
        }
        if (validate) {
            model.addFlashAttribute("user", user);
            model.addAttribute("url_action", Urls.AdminUsersSave);
            model.addAttribute("url_cancel", Urls.AdminUsersList);
//            ToastrNotificationUtils.addError(model, messageSource.getMessage(CommonMessages.msgFailedToSaveElement, new UserEntity[]{user}, LocaleContextHolder.getLocale()));
            return "redirect:" + Urls.AdminUsersCreate;

        }
        Boolean operation = userService.createOrUpdateByObject(user);
        if (operation) {
            logger.debug("[save] user save successfully ");
        }
        logger.info("[save] model : " + model);
        return Properties.UrlRedirect + Urls.AdminUsersList;
    }


}
