package com.datamond.crm.controller;


import com.datamond.crm.constant.Properties;
import com.datamond.crm.constant.Templates;
import com.datamond.crm.constant.Urls;
import com.datamond.crm.entity.Operator;
import com.datamond.crm.entity.Region;
import com.datamond.crm.repository.OperatorRepository;
import com.datamond.crm.repository.RegionRepository;
import com.datamond.crm.service.OperatorService;
import com.datamond.crm.service.RegionService;
import com.datamond.crm.utils.Breadcrumb;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class OperatorController {

    private Logger logger = LogManager.getLogger(OperatorController.class);

    private OperatorService operatorService;

    @Autowired
    public OperatorController(OperatorService operatorService) {
        this.operatorService = operatorService;
    }

    @RequestMapping(value = {Urls.ModelOperatorList})
    public String getOperatorList(Model model) {
        logger.info("[branch control] model: " + model);
        model.addAttribute("breadcrumb", getBreadcrumbForOperator("label.list"));
        model.addAttribute("url_edit", Urls.ModelOperatorEdit);
        model.addAttribute("url_view", Urls.ModelOperatorView);
        model.addAttribute("url_ajax", Urls.ModelOperatorListDataTable);
        model.addAttribute("url_add", Urls.ModelOperatorCreate);
        model.addAttribute("url_delete", Urls.ModelOperatorDelete);
        logger.info("[organization control] model: " + model);
        return Templates.ModelOperatorList;
    }


    @RequestMapping(value = Urls.ModelOperatorListDataTable, method = RequestMethod.GET)
    @ResponseBody
    public DataTablesOutput<Operator> getListAsDataTables(@Valid DataTablesInput input) {
        return operatorService.getDeletedTrueOperatorListInDataTableOutput(input);
    }

    @RequestMapping(value = Urls.ModelOperatorView+"/{id}", method = RequestMethod.GET)
    public String view(@PathVariable(name = "id", required = false) Long id,
                       Model model, RedirectAttributes redirectAttributes) {
        logger.info("[view] id = " + id);
        model.addAttribute("breadcrumb", getBreadcrumbForOperator("action.view"));
        model.addAttribute("url_cancel", Urls.ModelOperatorList);
        model.addAttribute("url_view", Urls.ModelOperatorView);
        model.addAttribute("url_edit", Urls.ModelOperatorEdit);
        model.addAttribute("url_add", Urls.ModelOperatorCreate + "/" + id);
//        model.addAttribute("employee", employee);
        logger.info("[view] model: " + model);
        return Templates.ModelOperatorView;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelOperatorCreate, method = RequestMethod.GET)
    public String create(Model model) {
        Operator operator;
        if (!model.containsAttribute("operator")){
            operator = new Operator();
            logger.debug("[create] added new operator: " + operator);
            model.addAttribute("operator", operator);
        }
        model.addAttribute("breadcrumb", getBreadcrumbForOperator("action.add"));
        model.addAttribute("url_action", Urls.ModelOperatorSave);
        model.addAttribute("url_cancel", Urls.ModelOperatorList);
        logger.info("[create] model: " + model);
        return Templates.ModelOperatorEdit;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelOperatorSave, method = RequestMethod.POST)
    public String save(Operator operator, RedirectAttributes model) {

        logger.info("[save] operator = " + operator);
        logger.info("[save] model = " + model);
        boolean validate = false;
        if (operator.getOperatorName()==null) {
            validate = true;
        }
        if (validate){
            model.addFlashAttribute("operator", operator);
            model.addAttribute("url_action", Urls.ModelOperatorSave);
            model.addAttribute("url_cancel", Urls.ModelOperatorList);
//            ToastrNotificationUtils.addError(model, messageSource.getMessage(CommonMessages.msgFailedToSaveElement, new OrganizationCategory[]{organizationCategory}, LocaleContextHolder.getLocale()));
            return "redirect:" + Urls.ModelOperatorCreate;

        }

        Boolean operation = operatorService.createOrUpdateByObject(operator);
        if (operation == Boolean.TRUE) {
            // success save
            logger.debug("[save] operator save successfully ");
//            ToastrNotificationUtils.addSuccess(model, messageSource.getMessage(CommonMessages.msgSavedSuccessfully, null, LocaleContextHolder.getLocale()));
        } else {
            // error
            logger.debug("[save] operator save error ");
//            ToastrNotificationUtils.addError(model, messageSource.getMessage(CommonMessages.msgFailedToSaveElement, null, LocaleContextHolder.getLocale()));
        }

        return Properties.UrlRedirect + Urls.ModelOperatorList;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelOperatorEdit+"/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable(name = "id", required = false) Long id, Model model, RedirectAttributes redirectAttributes) {

        logger.info("[edit] id = " + id);
        Operator operator = operatorService.findById(id);
        if (operator == null) {
            // not found
            logger.info("[save] requested operator not found = " + operator);
//            ToastrNotificationUtils.addWarning(redirectAttributes, messageSource.getMessage(CommonMessages.msgObjectNotFound, null, LocaleContextHolder.getLocale()));
            return Properties.UrlRedirect + Urls.ModelOperatorList;
        }

        model.addAttribute("breadcrumb", getBreadcrumbForOperator("label.edit"));
        model.addAttribute("operator", operator);
        model.addAttribute("url_action", Urls.ModelOperatorSave);
        model.addAttribute("url_cancel", Urls.ModelOperatorList);
        logger.info("[edit] model: " + model);
        return Templates.ModelOperatorEdit;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelOperatorDelete)
    public String delete(@RequestParam(name = "id", required = true) Long id,
                         RedirectAttributes redirectAttributes, Model model) {
        logger.info("[delete] id = " + id);
        Operator operator = operatorService.findById(id);
        if (operator == null) {
            // not found
            logger.info("[delete] requested operator not found: " + operator);
//            ToastrNotificationUtils.addWarning(redirectAttributes, messageSource.getMessage(CommonMessages.msgObjectNotFound, null, LocaleContextHolder.getLocale()));
            return Properties.UrlRedirect + Urls.ModelOperatorList;
        }
        operatorService.delete(operator);
        logger.debug("[delete] operator deleted: " + operator);
//        ToastrNotificationUtils.addSuccess(redirectAttributes, messageSource.getMessage(CommonMessages.msgDeletedSuccessfully, null, LocaleContextHolder.getLocale()));
        return Properties.UrlRedirect + Urls.ModelOperatorList;
    }

    public Breadcrumb getBreadcrumbForOperator(String name) {
        Breadcrumb breadcrumb = new Breadcrumb();
        breadcrumb.addLink("operator", Urls.ModelOperatorList);
        return breadcrumb;
    }
}