package com.datamond.crm.controller;


import com.datamond.crm.constant.*;
import com.datamond.crm.dto.dashboard.SimcardDtoForDashboard;
import com.datamond.crm.entity.*;
import com.datamond.crm.repository.*;
import com.datamond.crm.service.SimcardService;
import com.datamond.crm.utils.AuthUtils;
import com.datamond.crm.utils.Breadcrumb;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.thymeleaf.util.DateUtils;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class SimcardController {

    private Logger logger = LogManager.getLogger(SimcardController.class);

    private SimcardService simcardService;

    @Autowired
    private OperatorRepository operatorRepository;

    @Autowired
    private RegionRepository regionRepository;

    @Autowired
    private TarifRepository tarifRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private SimcardRepository simcardRepository;

    @Autowired
    public SimcardController(SimcardService simcardService) {
        this.simcardService = simcardService;
    }

    @RequestMapping(value = {Urls.ModelSimcardList})
    public String getSimcardList(Model model) {
        logger.info("[employee control] model: " + model);
        model.addAttribute("breadcrumb", getBreadcrumbForSimcard("label.list"));
        model.addAttribute("url_edit", Urls.ModelSimcardEdit);
        model.addAttribute("url_view", Urls.ModelSimcardView);
        model.addAttribute("url_ajax", Urls.ModelSimcardListDataTable);
        model.addAttribute("url_add", Urls.ModelSimcardCreate);
//        SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd");
//        String date1 = dtf.format(new Date());
        Long id = AuthUtils.getUserEntity().getBranchId();
//        Date dater = DateUtils.createToday().getTime();
//        Long ids = AuthUtils.getUserEntity().getBranchId();
//        Simcard simcard = simcardRepository.getSimcardByDeletedFalseAndRegisteredAtAndBranchId(DateUtils.createToday().getTime(), null);
//        model.addAttribute("counter", simcardRepository.countSimcardByDeletedFalseAndRegisteredAtAndBranchId(DateUtils.createToday().getTime(), null));
        List<SimcardDtoForDashboard> simcardData = simcardService.getData(AuthUtils.getUserId(), AuthUtils.getUserEntity().getBranchId());
        model.addAttribute("simcardData", simcardData);
        model.addAttribute("url_delete", Urls.ModelSimcardDelete);
        logger.info("[organization control] model: " + model);

        model.addAttribute("colors", getColors(simcardData));
        model.addAttribute("counters", getCounters(simcardData));
        model.addAttribute("names", getNames(simcardData));

        List<SimcardDtoForDashboard> simcardAllData = simcardService.getDataForComing(AuthUtils.getUserId(), AuthUtils.getUserEntity().getBranchId());
        model.addAttribute("simcardAllData", simcardAllData);
        model.addAttribute("date", "Oxirgi 30 kun");
        model.addAttribute("counter_data", getCounters(simcardAllData));
        model.addAttribute("names_data", getNames(simcardAllData));

        return Templates.ModelSimcardList;
    }

    public List<String> getColors(List<SimcardDtoForDashboard> sims){
        List<String> colors = new ArrayList<>();
        for (SimcardDtoForDashboard sim : sims){
            colors.add(sim.getColor());
        }
        return colors;
    }
    public List<Integer> getCounters(List<SimcardDtoForDashboard> sims){
        List<Integer> colors = new ArrayList<>();
        for (SimcardDtoForDashboard sim : sims){
            colors.add(sim.getCount());
        }
        return colors;
    }
    public List<String> getNames(List<SimcardDtoForDashboard> sims){
        List<String> colors = new ArrayList<>();
        for (SimcardDtoForDashboard sim : sims){
            colors.add(sim.getOperatorName());
        }
        return colors;
    }


    @RequestMapping(value = Urls.ModelSimcardListDataTable, method = RequestMethod.GET)
    @ResponseBody
    public DataTablesOutput<Simcard> getListAsDataTables(@Valid DataTablesInput input) {
        return simcardService.getDeletedTrueSimcardListInDataTableOutput(input, AuthUtils.getUserId());
    }

    @RequestMapping(value = Urls.ModelSimcardView+"/{id}", method = RequestMethod.GET)
    public String view(@PathVariable(name = "id", required = false) Long id,
                       Model model, RedirectAttributes redirectAttributes) {
//        Employee employee = employeeService.findById(id);
        logger.info("[view] id = " + id);
        model.addAttribute("breadcrumb", getBreadcrumbForSimcard("action.view"));
        model.addAttribute("url_cancel", Urls.ModelSimcardList);
        model.addAttribute("url_view", Urls.ModelSimcardView);
        model.addAttribute("url_edit", Urls.ModelSimcardEdit);
        model.addAttribute("url_add", Urls.ModelSimcardCreate + "/" + id);
//        model.addAttribute("employee", employee);
        logger.info("[view] model: " + model);
        return Templates.ModelSimcardView;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelSimcardCreate, method = RequestMethod.GET)
    public String create(Model model) {
        Simcard simcard;
//        Position position = positionService.findById(id);
        if (!model.containsAttribute("simcard")){
            simcard = new Simcard();
            logger.debug("[create] added new simcard: " + simcard);
            model.addAttribute("simcard", simcard);
        }
        List<Region> regions = regionRepository.findAll();
        model.addAttribute("regions", regions);

        List<Tarif> tarifs = tarifRepository.findAll();
        model.addAttribute("tarifs", tarifs);

        List<Operator> operators = operatorRepository.findAll();
        model.addAttribute("operators", operators);

        model.addAttribute("breadcrumb", getBreadcrumbForSimcard("action.add"));
        model.addAttribute("url_action", Urls.ModelSimcardSave);
        model.addAttribute("url_cancel", Urls.ModelSimcardList);
        logger.info("[create] model: " + model);
        return Templates.ModelSimcardEdit;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelSimcardSave, method = RequestMethod.POST)
    public String save(Simcard simcard, RedirectAttributes model) throws Exception{

        logger.info("[save] simcard = " + simcard);
        logger.info("[save] model = " + model);
        boolean validate = false;
        if (simcard.getAbonentFirstName()==null) {
            validate = true;
        }
        if (validate){
            model.addFlashAttribute("simcard", simcard);
            model.addAttribute("url_action", Urls.ModelSimcardSave);
            model.addAttribute("url_cancel", Urls.ModelSimcardList);
//            ToastrNotificationUtils.addError(model, messageSource.getMessage(CommonMessages.msgFailedToSaveElement, new OrganizationCategory[]{organizationCategory}, LocaleContextHolder.getLocale()));
            return "redirect:" + Urls.ModelSimcardCreate;

        }
//        Date date = DateParser.tryParse(birthDateString, Properties.uzbekistanDateFormat);
//        Date date = new SimpleDateFormat("dd/MM/yyyy").parse(birthDateString);
//        simcard.setBirthdate(date);
        User currentUser = AuthUtils.getUserEntity();
        Role role = userRoleRepository.findByUserId(currentUser.getId()).getRole();
        if (role==Role.BRANCH){
            simcard.setBranchId(currentUser.getBranchId());
        }
        Boolean operation = simcardService.createOrUpdateByObject(simcard);
        if (operation == Boolean.TRUE) {
            // success save
            logger.debug("[save] simcard save successfully ");
//            ToastrNotificationUtils.addSuccess(model, messageSource.getMessage(CommonMessages.msgSavedSuccessfully, null, LocaleContextHolder.getLocale()));
        } else {
            // error
            logger.debug("[save] simcard save error ");
//            ToastrNotificationUtils.addError(model, messageSource.getMessage(CommonMessages.msgFailedToSaveElement, null, LocaleContextHolder.getLocale()));
        }

        return Properties.UrlRedirect + Urls.ModelSimcardList;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelSimcardEdit+"/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable(name = "id", required = false) Long id, Model model, RedirectAttributes redirectAttributes) {

        logger.info("[edit] id = " + id);
        Simcard simcard = simcardService.findById(id);
        if (simcard == null) {
            // not found
            logger.info("[save] requested simcard not found = " + simcard);
//            ToastrNotificationUtils.addWarning(redirectAttributes, messageSource.getMessage(CommonMessages.msgObjectNotFound, null, LocaleContextHolder.getLocale()));
            return Properties.UrlRedirect + Urls.ModelSimcardList;
        }
        List<Region> regions = regionRepository.findAll();
        model.addAttribute("regions", regions);
        List<Tarif> tarifs = tarifRepository.findAll();
        model.addAttribute("tarifs", tarifs);
        List<Operator> operators = operatorRepository.findAll();
        model.addAttribute("operators", operators);
        model.addAttribute("breadcrumb", getBreadcrumbForSimcard("label.edit"));
        model.addAttribute("simcard", simcard);
        model.addAttribute("url_action", Urls.ModelSimcardSave);
        model.addAttribute("url_cancel", Urls.ModelSimcardList);
        logger.info("[edit] model: " + model);
        return Templates.ModelSimcardEdit;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelSimcardDelete)
    public String delete(@RequestParam(name = "id", required = true) Long id,
                         RedirectAttributes redirectAttributes, Model model) {
        logger.info("[delete] id = " + id);
        Simcard simcard = simcardService.findById(id);
        if (simcard == null) {
            // not found
            logger.info("[delete] requested simcard not found: " + simcard);
//            ToastrNotificationUtils.addWarning(redirectAttributes, messageSource.getMessage(CommonMessages.msgObjectNotFound, null, LocaleContextHolder.getLocale()));
            return Properties.UrlRedirect + Urls.ModelSimcardList;
        }
        simcardService.delete(simcard);
        logger.debug("[delete] simcard deleted: " + simcard);
//        ToastrNotificationUtils.addSuccess(redirectAttributes, messageSource.getMessage(CommonMessages.msgDeletedSuccessfully, null, LocaleContextHolder.getLocale()));
        return Properties.UrlRedirect + Urls.ModelSimcardList;
    }

    public Breadcrumb getBreadcrumbForSimcard(String name) {
        Breadcrumb breadcrumb = new Breadcrumb();
        breadcrumb.addLink("Simcard", Urls.ModelSimcardList);
        return breadcrumb;
    }



}