package com.datamond.crm.controller;


import com.datamond.crm.constant.Properties;
import com.datamond.crm.constant.Role;
import com.datamond.crm.constant.Templates;
import com.datamond.crm.constant.Urls;
import com.datamond.crm.dto.dashboard.PaynetDtoDebtPLan;
import com.datamond.crm.dto.dashboard.SimcardDtoForDashboard;
import com.datamond.crm.entity.*;
import com.datamond.crm.repository.*;
import com.datamond.crm.service.PaynetService;
import com.datamond.crm.service.SimcardService;
import com.datamond.crm.utils.AuthUtils;
import com.datamond.crm.utils.Breadcrumb;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class PaynetController {

    private Logger logger = LogManager.getLogger(PaynetController.class);

    private PaynetService paynetService;

    @Autowired
    private OperatorRepository operatorRepository;

    @Autowired
    private RegionRepository regionRepository;

    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    public PaynetController(PaynetService paynetService) {
        this.paynetService = paynetService;
    }

    @PreAuthorize("hasAuthority('ADMINISTRATOR')")
    @RequestMapping(value = {Urls.ModelPaynetList})
    public String getEmployeeList(Model model) {
        logger.info("[employee control] model: " + model);
        model.addAttribute("breadcrumb", getBreadcrumbForPaynet("label.list"));
        model.addAttribute("url_edit", Urls.ModelPaynetEdit);
        model.addAttribute("url_view", Urls.ModelPaynetView);
        model.addAttribute("url_ajax", Urls.ModelPaynetListDataTable);
        model.addAttribute("url_add", Urls.ModelSimcardCreate);
        model.addAttribute("url_delete", Urls.ModelPaynetDelete);
        logger.info("[organization control] model: " + model);

        List<PaynetDtoDebtPLan> paynets = paynetService.getDataPaynet();
        model.addAttribute("paynets", paynets);
        model.addAttribute("plans", getPlans(paynets));
        model.addAttribute("facts", getFacts(paynets));
        model.addAttribute("names", getNames(paynets));
        return Templates.ModelPaynetList;
    }

    public List<String> getNames(List<PaynetDtoDebtPLan> sims){
        List<String> names = new ArrayList<>();
        for (PaynetDtoDebtPLan sim : sims){
            names.add(sim.getName());
        }
        return names;
    }
    public List<Long> getPlans(List<PaynetDtoDebtPLan> sims){
        List<Long> plans = new ArrayList<>();
        for (PaynetDtoDebtPLan sim : sims){
            plans.add(sim.getPlan());
        }
        return plans;
    }
    public List<Long> getFacts(List<PaynetDtoDebtPLan> sims){
        List<Long> facts = new ArrayList<>();
        for (PaynetDtoDebtPLan sim : sims){
            facts.add(sim.getFact());
        }
        return facts;
    }


    @PreAuthorize("hasAuthority('ADMINISTRATOR')")
    @RequestMapping(value = Urls.ModelPaynetListDataTable, method = RequestMethod.GET)
    @ResponseBody
    public DataTablesOutput<Paynet> getListAsDataTables(@Valid DataTablesInput input) {
        return paynetService.getDeletedTruePaynetListInDataTableOutput(input);
    }

    @PreAuthorize("hasAuthority('ADMINISTRATOR')")
    @RequestMapping(value = Urls.ModelPaynetView+"/{id}", method = RequestMethod.GET)
    public String view(@PathVariable(name = "id", required = false) Long id,
                       Model model, RedirectAttributes redirectAttributes) {
        Paynet paynet = paynetService.findById(id);
        model.addAttribute("paynet", paynet);
        logger.info("[view] id = " + id);
        model.addAttribute("breadcrumb", getBreadcrumbForPaynet("action.view"));
        model.addAttribute("url_cancel", Urls.ModelPaynetList);
        model.addAttribute("url_view", Urls.ModelPaynetView);
        model.addAttribute("url_edit", Urls.ModelPaynetEdit);
        model.addAttribute("url_add", Urls.ModelPaynetCreate + "/" + id);
        logger.info("[view] model: " + model);
        return Templates.ModelPaynetView;
    }

    @PreAuthorize("hasAuthority('ADMINISTRATOR')")
    @RequestMapping(value = Urls.ModelPaynetCreate, method = RequestMethod.GET)
    public String create(Model model) {
        Paynet paynet;
        if (!model.containsAttribute("paynet")){
            paynet = new Paynet();
            logger.debug("[create] added new paynet: " + paynet);
            model.addAttribute("paynet", paynet);
        }
        List<Branch> branches = branchRepository.findAllByDeletedFalse();
        model.addAttribute("branches", branches);
        model.addAttribute("breadcrumb", getBreadcrumbForPaynet("action.add"));
        model.addAttribute("url_action", Urls.ModelPaynetSave);
        model.addAttribute("url_cancel", Urls.ModelPaynetList);
        logger.info("[create] model: " + model);
        return Templates.ModelPaynetEdit;
    }

    @PreAuthorize("hasAuthority('ADMINISTRATOR')")
    @RequestMapping(value = Urls.ModelPaynetSave, method = RequestMethod.POST)
    public String save(Paynet paynet, RedirectAttributes model) throws Exception{

        logger.info("[save] paynet = " + paynet);
        logger.info("[save] model = " + model);
        boolean validate = false;
        if (paynet.getPlan()==null) {
            validate = true;
        }
        if (validate){
            model.addFlashAttribute("paynet", paynet);
            model.addAttribute("url_action", Urls.ModelPaynetSave);
            model.addAttribute("url_cancel", Urls.ModelPaynetList);
//            ToastrNotificationUtils.addError(model, messageSource.getMessage(CommonMessages.msgFailedToSaveElement, new OrganizationCategory[]{organizationCategory}, LocaleContextHolder.getLocale()));
            return "redirect:" + Urls.ModelPaynetCreate;

        }
        Integer debt = paynet.getPlan() - paynet.getFact();
        paynet.setDebt(debt);
        Boolean operation = paynetService.createOrUpdateByObject(paynet);
        if (operation == Boolean.TRUE) {
            // success save
            logger.debug("[save] paynet save successfully ");
//            ToastrNotificationUtils.addSuccess(model, messageSource.getMessage(CommonMessages.msgSavedSuccessfully, null, LocaleContextHolder.getLocale()));
        } else {
            // error
            logger.debug("[save] paynet save error ");
//            ToastrNotificationUtils.addError(model, messageSource.getMessage(CommonMessages.msgFailedToSaveElement, null, LocaleContextHolder.getLocale()));
        }

        return Properties.UrlRedirect + Urls.ModelPaynetList;
    }

    @PreAuthorize("hasAuthority('ADMINISTRATOR')")
    @RequestMapping(value = Urls.ModelPaynetEdit+"/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable(name = "id", required = false) Long id, Model model, RedirectAttributes redirectAttributes) {

        logger.info("[edit] id = " + id);
        Paynet paynet = paynetService.findById(id);
        if (paynet == null) {
            // not found
            logger.info("[save] requested paynet not found = " + paynet);
//            ToastrNotificationUtils.addWarning(redirectAttributes, messageSource.getMessage(CommonMessages.msgObjectNotFound, null, LocaleContextHolder.getLocale()));
            return Properties.UrlRedirect + Urls.ModelPaynetList;
        }
        List<Branch> branches = branchRepository.findAllByDeletedFalse();
        model.addAttribute("branches", branches);
        model.addAttribute("breadcrumb", getBreadcrumbForPaynet("label.edit"));
        model.addAttribute("paynet", paynet);
        model.addAttribute("url_action", Urls.ModelPaynetSave);
        model.addAttribute("url_cancel", Urls.ModelPaynetList);
        logger.info("[edit] model: " + model);
        return Templates.ModelPaynetEdit;
    }

    @PreAuthorize("hasAuthority('ADMINISTRATOR')")
    @RequestMapping(value = Urls.ModelPaynetDelete)
    public String delete(@RequestParam(name = "id", required = true) Long id,
                         RedirectAttributes redirectAttributes, Model model) {
        logger.info("[delete] id = " + id);
        Paynet paynet = paynetService.findById(id);
        if (paynet == null) {
            // not found
            logger.info("[delete] requested paynet not found: " + paynet);
//            ToastrNotificationUtils.addWarning(redirectAttributes, messageSource.getMessage(CommonMessages.msgObjectNotFound, null, LocaleContextHolder.getLocale()));
            return Properties.UrlRedirect + Urls.ModelPaynetList;
        }
        paynetService.delete(paynet);
        logger.debug("[delete] paynet deleted: " + paynet);
//        ToastrNotificationUtils.addSuccess(redirectAttributes, messageSource.getMessage(CommonMessages.msgDeletedSuccessfully, null, LocaleContextHolder.getLocale()));
        return Properties.UrlRedirect + Urls.ModelPaynetList;
    }

    public Breadcrumb getBreadcrumbForPaynet(String name) {
        Breadcrumb breadcrumb = new Breadcrumb();
        breadcrumb.addLink("paynet", Urls.ModelPaynetList);
        return breadcrumb;
    }



}