package com.datamond.crm.controller;

import com.datamond.crm.dto.DataTableDto;
import com.datamond.crm.service.DataTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Locale;


@Controller
public class DataTableLangController {

    @Autowired
    DataTableService service;

    @RequestMapping(value = "/lang", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
    @ResponseBody
    public DataTableDto list(Locale locale) throws Exception {
        return service.get(locale.getLanguage());
    }

}