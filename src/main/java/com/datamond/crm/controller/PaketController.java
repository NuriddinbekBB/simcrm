package com.datamond.crm.controller;


import com.datamond.crm.constant.PaketTypes;
import com.datamond.crm.constant.Properties;
import com.datamond.crm.constant.Templates;
import com.datamond.crm.constant.Urls;
import com.datamond.crm.entity.Operator;
import com.datamond.crm.entity.Paket;
import com.datamond.crm.repository.OperatorRepository;
import com.datamond.crm.repository.PaketRepository;
import com.datamond.crm.service.PaketService;
import com.datamond.crm.utils.Breadcrumb;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

@Controller
public class PaketController {

    private Logger logger = LogManager.getLogger(PaketController.class);

    private PaketService paketService;

    @Autowired
    private OperatorRepository operatorRepository;

    @Autowired
    private PaketRepository paketRepository;

    @Autowired
    public PaketController(PaketService paketService) {
        this.paketService = paketService;
    }

    @RequestMapping(value = {Urls.ModelPaketList})
    public String getPaketList(Model model) {
        logger.info("[branch control] model: " + model);
        model.addAttribute("breadcrumb", getBreadcrumbForPaket("label.list"));
        model.addAttribute("url_edit", Urls.ModelPaketEdit);
        model.addAttribute("url_view", Urls.ModelPaketView);
        model.addAttribute("url_ajax", Urls.ModelPaketListDataTable);
        model.addAttribute("url_add", Urls.ModelPaketCreate);
        model.addAttribute("url_delete", Urls.ModelPaketDelete);
        logger.info("[organization control] model: " + model);
        return Templates.ModelPaketList;
    }


    @RequestMapping(value = Urls.ModelPaketListDataTable, method = RequestMethod.GET)
    @ResponseBody
    public DataTablesOutput<Paket> getListAsDataTables(@Valid DataTablesInput input) {
        return paketService.getDeletedTruePaketListInDataTableOutput(input);
    }

    @RequestMapping(value = Urls.ModelPaketView+"/{id}", method = RequestMethod.GET)
    public String view(@PathVariable(name = "id", required = false) Long id,
                       Model model, RedirectAttributes redirectAttributes) {
        logger.info("[view] id = " + id);
        model.addAttribute("breadcrumb", getBreadcrumbForPaket("action.view"));
        model.addAttribute("url_cancel", Urls.ModelPaketList);
        model.addAttribute("url_view", Urls.ModelPaketView);
        model.addAttribute("url_edit", Urls.ModelPaketEdit);
        model.addAttribute("url_add", Urls.ModelPaketCreate + "/" + id);
        logger.info("[view] model: " + model);
        return Templates.ModelPaketView;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelPaketCreate, method = RequestMethod.GET)
    public String create(Model model) {
        Paket paket;
        if (!model.containsAttribute("paket")){
            paket = new Paket();
            logger.debug("[create] added new paket: " + paket);
            model.addAttribute("paket", paket);
        }
        List<Operator> operators = operatorRepository.findAll();
        model.addAttribute("opers", operators);
        PaketTypes[] types = PaketTypes.values();
        model.addAttribute("types", types);
        model.addAttribute("breadcrumb", getBreadcrumbForPaket("action.add"));
        model.addAttribute("url_action", Urls.ModelPaketSave);
        model.addAttribute("url_cancel", Urls.ModelPaketList);
        logger.info("[create] model: " + model);
        return Templates.ModelPaketEdit;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelPaketSave, method = RequestMethod.POST)
    public String save(Paket paket, RedirectAttributes model) {

        logger.info("[save] paket = " + paket);
        logger.info("[save] model = " + model);
        boolean validate = false;
        if (paket.getPaketName()==null) {
            validate = true;
        }
        if (validate){
            model.addFlashAttribute("paket", paket);
            model.addAttribute("url_action", Urls.ModelPaketSave);
            model.addAttribute("url_cancel", Urls.ModelPaketList);
//            ToastrNotificationUtils.addError(model, messageSource.getMessage(CommonMessages.msgFailedToSaveElement, new OrganizationCategory[]{organizationCategory}, LocaleContextHolder.getLocale()));
            return "redirect:" + Urls.ModelPaketCreate;

        }

        Boolean operation = paketService.createOrUpdateByObject(paket);
        if (operation == Boolean.TRUE) {
            // success save
            logger.debug("[save] paket save successfully ");
//            ToastrNotificationUtils.addSuccess(model, messageSource.getMessage(CommonMessages.msgSavedSuccessfully, null, LocaleContextHolder.getLocale()));
        } else {
            // error
            logger.debug("[save] paket save error ");
//            ToastrNotificationUtils.addError(model, messageSource.getMessage(CommonMessages.msgFailedToSaveElement, null, LocaleContextHolder.getLocale()));
        }

        return Properties.UrlRedirect + Urls.ModelPaketList;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelPaketEdit+"/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable(name = "id", required = false) Long id, Model model, RedirectAttributes redirectAttributes) {

        logger.info("[edit] id = " + id);
        Paket paket = paketService.findById(id);
        if (paket == null) {
            // not found
            logger.info("[save] requested paket not found = " + paket);
//            ToastrNotificationUtils.addWarning(redirectAttributes, messageSource.getMessage(CommonMessages.msgObjectNotFound, null, LocaleContextHolder.getLocale()));
            return Properties.UrlRedirect + Urls.ModelPaketList;
        }
        List<Operator> operators = operatorRepository.findAll();
        model.addAttribute("opers", operators);
        PaketTypes[] types = PaketTypes.values();
        model.addAttribute("types", types);
        model.addAttribute("breadcrumb", getBreadcrumbForPaket("label.edit"));
        model.addAttribute("paket", paket);
        model.addAttribute("url_action", Urls.ModelPaketSave);
        model.addAttribute("url_cancel", Urls.ModelPaketList);
        logger.info("[edit] model: " + model);
        return Templates.ModelPaketEdit;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelPaketDelete)
    public String delete(@RequestParam(name = "id", required = true) Long id,
                         RedirectAttributes redirectAttributes, Model model) {
        logger.info("[delete] id = " + id);
        Paket paket = paketService.findById(id);
        if (paket == null) {
            // not found
            logger.info("[delete] requested paket not found: " + paket);
//            ToastrNotificationUtils.addWarning(redirectAttributes, messageSource.getMessage(CommonMessages.msgObjectNotFound, null, LocaleContextHolder.getLocale()));
            return Properties.UrlRedirect + Urls.ModelPaketList;
        }
        paketService.delete(paket);
        logger.debug("[delete] paket deleted: " + paket);
//        ToastrNotificationUtils.addSuccess(redirectAttributes, messageSource.getMessage(CommonMessages.msgDeletedSuccessfully, null, LocaleContextHolder.getLocale()));
        return Properties.UrlRedirect + Urls.ModelPaketList;
    }

    public Breadcrumb getBreadcrumbForPaket(String name) {
        Breadcrumb breadcrumb = new Breadcrumb();
        breadcrumb.addLink("paket", Urls.ModelPaketList);
        return breadcrumb;
    }



}