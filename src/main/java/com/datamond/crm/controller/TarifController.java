package com.datamond.crm.controller;


import com.datamond.crm.constant.Properties;
import com.datamond.crm.constant.Templates;
import com.datamond.crm.constant.Urls;
import com.datamond.crm.entity.Operator;
import com.datamond.crm.entity.Tarif;
import com.datamond.crm.repository.OperatorRepository;
import com.datamond.crm.repository.TarifRepository;
import com.datamond.crm.service.TarifService;
import com.datamond.crm.utils.Breadcrumb;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

@Controller
public class TarifController {

    private Logger logger = LogManager.getLogger(TarifController.class);

    private TarifService tarifService;

    @Autowired
    private OperatorRepository operatorRepository;

    @Autowired
    private TarifRepository tarifRepository;

    @Autowired
    public TarifController(TarifService tarifService) {
        this.tarifService = tarifService;
    }

    @RequestMapping(value = {Urls.ModelTarifList})
    public String getTarifList(Model model) {
        logger.info("[branch control] model: " + model);
        model.addAttribute("breadcrumb", getBreadcrumbForTarif("label.list"));
        model.addAttribute("url_edit", Urls.ModelTarifEdit);
        model.addAttribute("url_view", Urls.ModelTarifView);
        model.addAttribute("url_ajax", Urls.ModelTarifListDataTable);
        model.addAttribute("url_add", Urls.ModelTarifCreate);
        model.addAttribute("url_delete", Urls.ModelTarifDelete);
        logger.info("[organization control] model: " + model);
        return Templates.ModelTarifList;
    }


    @RequestMapping(value = Urls.ModelTarifListDataTable, method = RequestMethod.GET)
    @ResponseBody
    public DataTablesOutput<Tarif> getListAsDataTables(@Valid DataTablesInput input) {
        return tarifService.getDeletedTrueTarifListInDataTableOutput(input);
    }

    @RequestMapping(value = Urls.ModelTarifView+"/{id}", method = RequestMethod.GET)
    public String view(@PathVariable(name = "id", required = false) Long id,
                       Model model, RedirectAttributes redirectAttributes) {
        logger.info("[view] id = " + id);
        model.addAttribute("breadcrumb", getBreadcrumbForTarif("action.view"));
        model.addAttribute("url_cancel", Urls.ModelTarifList);
        model.addAttribute("url_view", Urls.ModelTarifView);
        model.addAttribute("url_edit", Urls.ModelTarifEdit);
        model.addAttribute("url_add", Urls.ModelTarifCreate + "/" + id);
        logger.info("[view] model: " + model);
        return Templates.ModelTarifView;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelTarifCreate, method = RequestMethod.GET)
    public String create(Model model) {
        Tarif tarif;
        if (!model.containsAttribute("tarif")){
            tarif = new Tarif();
            logger.debug("[create] added new tarif: " + tarif);
            model.addAttribute("tarif", tarif);
        }
        List<Operator> operators = operatorRepository.findAll();
        model.addAttribute("opers", operators);
        model.addAttribute("breadcrumb", getBreadcrumbForTarif("action.add"));
        model.addAttribute("url_action", Urls.ModelTarifSave);
        model.addAttribute("url_cancel", Urls.ModelTarifList);
        logger.info("[create] model: " + model);
        return Templates.ModelTarifEdit;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelTarifSave, method = RequestMethod.POST)
    public String save(Tarif tarif, RedirectAttributes model) {

        logger.info("[save] tarif = " + tarif);
        logger.info("[save] model = " + model);
        boolean validate = false;
        if (tarif.getTarifName()==null) {
            validate = true;
        }
        if (validate){
            model.addFlashAttribute("tarif", tarif);
            model.addAttribute("url_action", Urls.ModelTarifSave);
            model.addAttribute("url_cancel", Urls.ModelTarifList);
//            ToastrNotificationUtils.addError(model, messageSource.getMessage(CommonMessages.msgFailedToSaveElement, new OrganizationCategory[]{organizationCategory}, LocaleContextHolder.getLocale()));
            return "redirect:" + Urls.ModelTarifCreate;

        }

        Boolean operation = tarifService.createOrUpdateByObject(tarif);
        if (operation == Boolean.TRUE) {
            // success save
            logger.debug("[save] tarif save successfully ");
//            ToastrNotificationUtils.addSuccess(model, messageSource.getMessage(CommonMessages.msgSavedSuccessfully, null, LocaleContextHolder.getLocale()));
        } else {
            // error
            logger.debug("[save] tarif save error ");
//            ToastrNotificationUtils.addError(model, messageSource.getMessage(CommonMessages.msgFailedToSaveElement, null, LocaleContextHolder.getLocale()));
        }

        return Properties.UrlRedirect + Urls.ModelTarifList;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelTarifEdit+"/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable(name = "id", required = false) Long id, Model model, RedirectAttributes redirectAttributes) {

        logger.info("[edit] id = " + id);
        Tarif tarif = tarifService.findById(id);
        if (tarif == null) {
            // not found
            logger.info("[save] requested tarif not found = " + tarif);
//            ToastrNotificationUtils.addWarning(redirectAttributes, messageSource.getMessage(CommonMessages.msgObjectNotFound, null, LocaleContextHolder.getLocale()));
            return Properties.UrlRedirect + Urls.ModelTarifList;
        }
        List<Operator> operators = operatorRepository.findAll();
        model.addAttribute("opers", operators);
        model.addAttribute("breadcrumb", getBreadcrumbForTarif("label.edit"));
        model.addAttribute("tarif", tarif);
        model.addAttribute("url_action", Urls.ModelTarifSave);
        model.addAttribute("url_cancel", Urls.ModelTarifList);
        logger.info("[edit] model: " + model);
        return Templates.ModelTarifEdit;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelTarifDelete)
    public String delete(@RequestParam(name = "id", required = true) Long id,
                         RedirectAttributes redirectAttributes, Model model) {
        logger.info("[delete] id = " + id);
        Tarif tarif = tarifService.findById(id);
        if (tarif == null) {
            // not found
            logger.info("[delete] requested tarif not found: " + tarif);
//            ToastrNotificationUtils.addWarning(redirectAttributes, messageSource.getMessage(CommonMessages.msgObjectNotFound, null, LocaleContextHolder.getLocale()));
            return Properties.UrlRedirect + Urls.ModelTarifList;
        }
        tarifService.delete(tarif);
        logger.debug("[delete] tarif deleted: " + tarif);
//        ToastrNotificationUtils.addSuccess(redirectAttributes, messageSource.getMessage(CommonMessages.msgDeletedSuccessfully, null, LocaleContextHolder.getLocale()));
        return Properties.UrlRedirect + Urls.ModelTarifList;
    }

    public Breadcrumb getBreadcrumbForTarif(String name) {
        Breadcrumb breadcrumb = new Breadcrumb();
        breadcrumb.addLink("tarif", Urls.ModelTarifList);
        return breadcrumb;
    }



}