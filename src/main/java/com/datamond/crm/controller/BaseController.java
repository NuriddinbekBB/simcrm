package com.datamond.crm.controller;

import com.datamond.crm.constant.Urls;
import com.datamond.crm.service.CustomSuccessHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;

@Controller
public class BaseController {
    private CustomSuccessHandler customSuccessHandler;
    private Logger logger = LogManager.getLogger(BaseController.class);

    @Autowired
    public void setCustomSuccessHandler(CustomSuccessHandler customSuccessHandler) {
//        this.customSuccessHandler = customSuccessHandler;
        this.customSuccessHandler = customSuccessHandler;
    }

    @RequestMapping(value = Urls.ErrorPage403)
    public String error403() {
        logger.info("[403 error]");
        return "403";
    }

    @RequestMapping(value = Urls.ErrorPage404)
    public String error404() {
        logger.info("[404 error]");
        return "404";
    }

    @RequestMapping(value = Urls.ErrorPage500)
    public String error500() {
        logger.info("[500 error]");
        return "500";
    }

    @RequestMapping("/")
    public String index() {
        logger.info("[login]");
        return "redirect:/login";
    }

    @RequestMapping(value = {"/login"})
    public String login(Principal principal) throws NullPointerException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String url = customSuccessHandler.determineTargetUrl(auth);

        if (principal != null) {
            logger.info("login redirect: " + url);
            return "redirect:" + url;
        } else {
            logger.info("login successful");
            return "login";
        }
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        assert auth != null;
        return "redirect:/";
    }
}