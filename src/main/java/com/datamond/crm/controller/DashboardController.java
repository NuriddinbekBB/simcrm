package com.datamond.crm.controller;

import com.datamond.crm.constant.Templates;
import com.datamond.crm.constant.Urls;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DashboardController {

    private Logger logger = LogManager.getLogger(DashboardController.class);

    @RequestMapping(value = {Urls.AdminDashboard, Urls.ModelDashboard})
    public String adminDashboard(Model model) {

        model.addAttribute("url_profile", Urls.AdminUsersProfile);

        logger.info("[adminDashboard] model: " + model);
        return Templates.AdminDashboard;
    }

}