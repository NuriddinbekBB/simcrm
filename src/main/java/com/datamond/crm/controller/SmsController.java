package com.datamond.crm.controller;


import com.datamond.crm.constant.PaketTypes;
import com.datamond.crm.constant.Properties;
import com.datamond.crm.constant.Templates;
import com.datamond.crm.constant.Urls;
import com.datamond.crm.entity.Operator;
import com.datamond.crm.entity.Paket;
import com.datamond.crm.entity.Sms;
import com.datamond.crm.repository.OperatorRepository;
import com.datamond.crm.repository.PaketRepository;
import com.datamond.crm.repository.SmsRepository;
import com.datamond.crm.service.PaketService;
import com.datamond.crm.service.SmsService;
import com.datamond.crm.utils.Breadcrumb;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

@Controller
public class SmsController {

    private Logger logger = LogManager.getLogger(SmsController.class);

    private SmsService smsService;

    @Autowired
    private OperatorRepository operatorRepository;

    @Autowired
    private SmsRepository smsRepository;

    @Autowired
    public SmsController(SmsService smsService) {
        this.smsService = smsService;
    }

    @RequestMapping(value = {Urls.ModelSmsList})
    public String getPaketList(Model model) {
        logger.info("[branch control] model: " + model);
        model.addAttribute("breadcrumb", getBreadcrumbForPaket("label.list"));
        model.addAttribute("url_edit", Urls.ModelSmsEdit);
        model.addAttribute("url_view", Urls.ModelSmsView);
        model.addAttribute("url_ajax", Urls.ModelSmsListDataTable);
        model.addAttribute("url_add", Urls.ModelSmsCreate);
        model.addAttribute("url_delete", Urls.ModelSmsDelete);
        logger.info("[organization control] model: " + model);
        return Templates.ModelSmsList;
    }


    @RequestMapping(value = Urls.ModelSmsListDataTable, method = RequestMethod.GET)
    @ResponseBody
    public DataTablesOutput<Sms> getListAsDataTables(@Valid DataTablesInput input) {
        return smsService.getDeletedTrueSmsListInDataTableOutput(input);
    }

    @RequestMapping(value = Urls.ModelSmsView+"/{id}", method = RequestMethod.GET)
    public String view(@PathVariable(name = "id", required = false) Long id,
                       Model model, RedirectAttributes redirectAttributes) {
        logger.info("[view] id = " + id);
        model.addAttribute("breadcrumb", getBreadcrumbForPaket("action.view"));
        model.addAttribute("url_cancel", Urls.ModelSmsList);
        model.addAttribute("url_view", Urls.ModelSmsView);
        model.addAttribute("url_edit", Urls.ModelSmsEdit);
        model.addAttribute("url_add", Urls.ModelSmsCreate + "/" + id);
        logger.info("[view] model: " + model);
        return Templates.ModelSmsView;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelSmsCreate, method = RequestMethod.GET)
    public String create(Model model) {
        Sms sms;
        if (!model.containsAttribute("sms")){
            sms = new Sms();
            logger.debug("[create] added new sms: " + sms);
            model.addAttribute("sms", sms);
        }
        List<Operator> operators = operatorRepository.findAll();
        model.addAttribute("opers", operators);
        PaketTypes[] types = PaketTypes.values();
        model.addAttribute("types", types);
        model.addAttribute("breadcrumb", getBreadcrumbForPaket("action.add"));
        model.addAttribute("url_action", Urls.ModelSmsSave);
        model.addAttribute("url_cancel", Urls.ModelSmsList);
        logger.info("[create] model: " + model);
        return Templates.ModelSmsEdit;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelSmsSave, method = RequestMethod.POST)
    public String save(Sms sms, RedirectAttributes model) {

        logger.info("[save] sms = " + sms);
        logger.info("[save] model = " + model);
        boolean validate = false;
        if (sms.getPaketName()==null) {
            validate = true;
        }
        if (validate){
            model.addFlashAttribute("sms", sms);
            model.addAttribute("url_action", Urls.ModelSmsSave);
            model.addAttribute("url_cancel", Urls.ModelSmsList);
//            ToastrNotificationUtils.addError(model, messageSource.getMessage(CommonMessages.msgFailedToSaveElement, new OrganizationCategory[]{organizationCategory}, LocaleContextHolder.getLocale()));
            return "redirect:" + Urls.ModelSmsCreate;

        }

        Boolean operation = smsService.createOrUpdateByObject(sms);
        if (operation == Boolean.TRUE) {
            // success save
            logger.debug("[save] sms save successfully ");
//            ToastrNotificationUtils.addSuccess(model, messageSource.getMessage(CommonMessages.msgSavedSuccessfully, null, LocaleContextHolder.getLocale()));
        } else {
            // error
            logger.debug("[save] sms save error ");
//            ToastrNotificationUtils.addError(model, messageSource.getMessage(CommonMessages.msgFailedToSaveElement, null, LocaleContextHolder.getLocale()));
        }

        return Properties.UrlRedirect + Urls.ModelSmsList;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelSmsEdit+"/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable(name = "id", required = false) Long id, Model model, RedirectAttributes redirectAttributes) {

        logger.info("[edit] id = " + id);
        Sms sms = smsService.findById(id);
        if (sms == null) {
            // not found
            logger.info("[save] requested sms not found = " + sms);
//            ToastrNotificationUtils.addWarning(redirectAttributes, messageSource.getMessage(CommonMessages.msgObjectNotFound, null, LocaleContextHolder.getLocale()));
            return Properties.UrlRedirect + Urls.ModelSmsList;
        }
        List<Operator> operators = operatorRepository.findAll();
        model.addAttribute("opers", operators);
        PaketTypes[] types = PaketTypes.values();
        model.addAttribute("types", types);
        model.addAttribute("breadcrumb", getBreadcrumbForPaket("label.edit"));
        model.addAttribute("sms", sms);
        model.addAttribute("url_action", Urls.ModelSmsSave);
        model.addAttribute("url_cancel", Urls.ModelSmsList);
        logger.info("[edit] model: " + model);
        return Templates.ModelSmsEdit;
    }

//    @PreAuthorize("hasAuthority('BRANCH')")
    @RequestMapping(value = Urls.ModelSmsDelete)
    public String delete(@RequestParam(name = "id", required = true) Long id,
                         RedirectAttributes redirectAttributes, Model model) {
        logger.info("[delete] id = " + id);
        Sms sms = smsService.findById(id);
        if (sms == null) {
            // not found
            logger.info("[delete] requested sms not found: " + sms);
//            ToastrNotificationUtils.addWarning(redirectAttributes, messageSource.getMessage(CommonMessages.msgObjectNotFound, null, LocaleContextHolder.getLocale()));
            return Properties.UrlRedirect + Urls.ModelSmsList;
        }
        smsService.delete(sms);
        logger.debug("[delete] sms deleted: " + sms);
//        ToastrNotificationUtils.addSuccess(redirectAttributes, messageSource.getMessage(CommonMessages.msgDeletedSuccessfully, null, LocaleContextHolder.getLocale()));
        return Properties.UrlRedirect + Urls.ModelSmsList;
    }

    public Breadcrumb getBreadcrumbForPaket(String name) {
        Breadcrumb breadcrumb = new Breadcrumb();
        breadcrumb.addLink("sms", Urls.ModelSmsList);
        return breadcrumb;
    }



}