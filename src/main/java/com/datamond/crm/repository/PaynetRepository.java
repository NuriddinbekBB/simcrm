package com.datamond.crm.repository;

import com.datamond.crm.dto.dashboard.PaynetDtoDebt;
import com.datamond.crm.entity.Branch;
import com.datamond.crm.entity.Paynet;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;


@Repository
public interface PaynetRepository extends DataTablesRepository<Paynet, Integer>, JpaRepository<Paynet, Integer> {

    Paynet findById(Long id);
    List<Paynet> findAllByDeletedFalse();

    @Query(value = "SELECT new com.datamond.crm.dto.dashboard.PaynetDtoDebt(SUM(p.plan), SUM(p.fact)) "+"FROM Paynet p WHERE p.branchId=:branchId and p.givenDate=:date")
    PaynetDtoDebt getPaynetForDashboard(@Param("branchId") Long branchId, @Param("date") Date date);

}
