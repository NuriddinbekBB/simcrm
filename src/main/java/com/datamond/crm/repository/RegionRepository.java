package com.datamond.crm.repository;

import com.datamond.crm.entity.Branch;
import com.datamond.crm.entity.Region;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RegionRepository extends DataTablesRepository<Region, Integer>, JpaRepository<Region, Integer> {

    Region findById(Long id);

}
