package com.datamond.crm.repository;

import com.datamond.crm.entity.Paket;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface PaketRepository extends DataTablesRepository<Paket, Integer>, JpaRepository<Paket, Integer> {

    Paket findById(Long id);
    List<Paket> findAllByType(String type);

}
