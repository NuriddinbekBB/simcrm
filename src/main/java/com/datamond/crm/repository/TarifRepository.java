package com.datamond.crm.repository;

import com.datamond.crm.entity.Region;
import com.datamond.crm.entity.Tarif;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TarifRepository extends DataTablesRepository<Tarif, Integer>, JpaRepository<Tarif, Integer> {

    Tarif findById(Long id);

}
