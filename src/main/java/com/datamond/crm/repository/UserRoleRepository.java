package com.datamond.crm.repository;

import com.datamond.crm.entity.UserRole;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRoleRepository extends DataTablesRepository<UserRole, Long>, JpaRepository<UserRole, Long> {

    UserRole findByUserId(Long id);
}
