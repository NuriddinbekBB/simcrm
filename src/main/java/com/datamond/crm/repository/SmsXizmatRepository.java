package com.datamond.crm.repository;

import com.datamond.crm.entity.SmsXizmat;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;


@Repository
public interface SmsXizmatRepository extends DataTablesRepository<SmsXizmat, Integer>, JpaRepository<SmsXizmat, Integer> {

    SmsXizmat findById(Long id);

    Integer countServiceByDeletedFalseAndActiveTrue();

    Integer countSmsXizmatByDeletedFalseAndEnabledAtAndBranchIdAndOperatorId(Date date, Long branchId, Long operatorId);
    Integer countSmsXizmatByDeletedFalseAndEnabledAtAndOperatorId(Date date, Long operatorId);

    @Query(value = "SELECT COUNT(s) FROM Service s WHERE s.enabled_at > CURRENT_DATE - INTERVAL '1 month' AND s.branch_id=:branchId AND s.operator_id=:operatorId AND s.deleted=false", nativeQuery = true)
    Integer getDashboardService(@Param("branchId") Long branchId, @Param("operatorId") Long operatorId);

    @Query(value = "SELECT COUNT(s) FROM Service s WHERE s.enabled_at > CURRENT_DATE - INTERVAL '1 month' AND s.operator_id=:operatorId AND s.deleted=false", nativeQuery = true)
    Integer getDashboardServiceAdmin(@Param("operatorId") Long operatorId);
}
