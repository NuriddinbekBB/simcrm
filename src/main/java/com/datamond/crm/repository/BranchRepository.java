package com.datamond.crm.repository;

import com.datamond.crm.entity.Branch;
import com.datamond.crm.entity.Simcard;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface BranchRepository extends DataTablesRepository<Branch, Integer>, JpaRepository<Branch, Integer> {

    Branch findById(Long id);
    List<Branch> findAllByDeletedFalse();

}
