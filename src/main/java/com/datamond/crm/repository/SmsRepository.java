package com.datamond.crm.repository;

import com.datamond.crm.entity.Paket;
import com.datamond.crm.entity.Sms;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface SmsRepository extends DataTablesRepository<Sms, Integer>, JpaRepository<Sms, Integer> {

    Sms findById(Long id);
    List<Sms> findAllByDeletedFalse();

}
