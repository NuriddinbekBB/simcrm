package com.datamond.crm.repository;

import com.datamond.crm.dto.dashboard.SimcardDto;
import com.datamond.crm.entity.Simcard;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;


@Repository
public interface SimcardRepository extends DataTablesRepository<Simcard, Integer>, JpaRepository<Simcard, Integer> {

    Simcard findById(Long id);

    Integer countSimcardByDeletedFalseAndActiveTrue();

    Integer countSimcardByDeletedFalseAndRegisteredAtAndBranchId(Date date,Long branchId);
    Integer countSimcardByDeletedFalseAndRegisteredAtAndBranchIdAndOperatorId(Date date, Long branchId, Long operatorId);
    Integer countSimcardByDeletedFalseAndRegisteredAtAndOperatorId(Date date, Long operatorId);
    @Query(value = "SELECT COUNT(s) FROM Simcard s WHERE s.registered_at > CURRENT_DATE - INTERVAL '1 month' AND s.branch_id=:branchId AND s.operator_id=:operatorId AND s.deleted=false", nativeQuery = true)
    Integer getDashboardSimcard(@Param("branchId") Long branchId, @Param("operatorId") Long operatorId);


    @Query(value = "SELECT COUNT(s) FROM Simcard s WHERE s.registered_at > CURRENT_DATE - INTERVAL '1 month' AND s.operator_id=:operatorId AND s.deleted=false", nativeQuery = true)
    Integer getDashboardSimcardAdmin(@Param("operatorId") Long operatorId);

}