package com.datamond.crm.repository;

import com.datamond.crm.entity.Operator;
import com.datamond.crm.entity.Simcard;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface OperatorRepository extends DataTablesRepository<Operator, Integer>, JpaRepository<Operator, Integer> {

    Operator findById(Long id);
    List<Operator> findAllByDeletedFalse();

}
