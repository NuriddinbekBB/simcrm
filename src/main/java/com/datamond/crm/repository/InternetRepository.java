package com.datamond.crm.repository;

import com.datamond.crm.entity.Internet;
import com.datamond.crm.entity.Sms;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface InternetRepository extends DataTablesRepository<Internet, Integer>, JpaRepository<Internet, Integer> {

    Internet findById(Long id);
    List<Internet> findAllByDeletedFalse();

}
