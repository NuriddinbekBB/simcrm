package com.datamond.crm.repository;

import com.datamond.crm.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface ProductRepo extends JpaRepository<Product,Integer> {

    @Modifying
    @Transactional
    @Query(value = "update product  set quantity=quantity-?1 " +
            "                            where id = ?2",nativeQuery = true)
    void updateQuantity(Integer quantity,Integer id);


}
