package com.datamond.crm.utils;

import java.text.SimpleDateFormat;
import java.util.Date;


public class DateParser {

    public static Date tryParse(String dateAsString, SimpleDateFormat dateFormat) {
        try{
            return dateFormat.parse(dateAsString);
        }
        catch (Exception ignored) {

        }
        return null;
    }
}
