package com.datamond.crm.utils;


import com.datamond.crm.entity.User;
import com.datamond.crm.entity.UserRole;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class ProjectUserDetails implements UserDetails {

    private final User userEntity;
    private final Long userId;
    private Set<Long> orgIdbyUser;
    public ProjectUserDetails(User userEntity) {
        this.userEntity = userEntity;
        this.userId = userEntity.getId();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        System.out.println(" getAuth ");
        Set<GrantedAuthority> authoritySet = new HashSet<GrantedAuthority>();
        orgIdbyUser = new HashSet<>();
        for (UserRole userRole: userEntity.getRoles()){
            System.out.println(" role : " + userRole.getRole());
            if (userRole.getEnabled()){
                authoritySet.add(new SimpleGrantedAuthority(userRole.getRole().name()));
            }
        }
        return new ArrayList<GrantedAuthority>(authoritySet);
    }

    @Override
    public String getPassword() {
        return userEntity.getPassword();
    }

    @Override
    public String getUsername() {
        return userEntity.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return userEntity.getEnabled();
    }

    public Set<UserRole> userRoles(){
        return userEntity.getRoles();
    }

    public User getUserEntity() {
        return userEntity;
    }

    public Long getUserId() {
        return userId;
    }
}
