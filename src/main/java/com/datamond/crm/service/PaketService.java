package com.datamond.crm.service;
import com.datamond.crm.entity.Paket;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

public interface PaketService {
    DataTablesOutput<Paket> getDeletedTruePaketListInDataTableOutput(DataTablesInput input);
    Paket findById(Long id);
    Boolean createOrUpdateByObject(Paket paket);
    void delete(Paket paket);
    
}
