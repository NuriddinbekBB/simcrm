package com.datamond.crm.service;
import com.datamond.crm.entity.Internet;
import com.datamond.crm.entity.Sms;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

public interface InternetService {
    DataTablesOutput<Internet> getDeletedTrueInternetListInDataTableOutput(DataTablesInput input);
    Internet findById(Long id);
    Boolean createOrUpdateByObject(Internet internet);
    void delete(Internet internet);
}