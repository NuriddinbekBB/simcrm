package com.datamond.crm.service;
import com.datamond.crm.dto.dashboard.SimcardDtoForDashboard;
import com.datamond.crm.dto.dashboard.SmsDtoForDashboard;
import com.datamond.crm.entity.SmsXizmat;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

import java.util.List;

public interface SmsXizmatService {
    DataTablesOutput<SmsXizmat> getDeletedTrueServiceListInDataTableOutput(DataTablesInput input, Long id);
    SmsXizmat findById(Long id);
    Boolean createOrUpdateByObject(SmsXizmat service);
    void delete(SmsXizmat service);


    List<SmsDtoForDashboard> getData(Long userId, Long branchId);

    List<SmsDtoForDashboard> getDataForComing(Long userId, Long branchId);
}
