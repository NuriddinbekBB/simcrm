package com.datamond.crm.service;

import com.datamond.crm.dto.UserData;
import com.datamond.crm.entity.Region;
import com.datamond.crm.entity.User;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

public interface UserService {
    void delete(User user);
    DataTablesOutput<User> getDeletedTrueUserListInDataTableOutput(DataTablesInput input);
    void register(UserData user) throws Exception;

    boolean checkIfUserExist(String email);
    User findById(Long id);
    Boolean createOrUpdateByObject(User user);
}
