package com.datamond.crm.service;
import com.datamond.crm.entity.Operator;
import com.datamond.crm.entity.Region;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

import java.util.List;

public interface OperatorService {
    DataTablesOutput<Operator> getDeletedTrueOperatorListInDataTableOutput(DataTablesInput input);
    Operator findById(Long id);
    Boolean createOrUpdateByObject(Operator operator);
    void delete(Operator operator);
    List<Operator> findAll();
    
}
