package com.datamond.crm.service;
import com.datamond.crm.dto.dashboard.PaynetDtoDebt;
import com.datamond.crm.dto.dashboard.PaynetDtoDebtPLan;
import com.datamond.crm.dto.dashboard.SimcardDtoForDashboard;
import com.datamond.crm.entity.Branch;
import com.datamond.crm.entity.Paynet;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

import java.util.List;

public interface PaynetService {
    DataTablesOutput<Paynet> getDeletedTruePaynetListInDataTableOutput(DataTablesInput input);
    Paynet findById(Long id);
    List<Paynet> findAll();
    Boolean createOrUpdateByObject(Paynet paynet);
    void delete(Paynet paynet);


    List<PaynetDtoDebtPLan> getDataPaynet();
}
