package com.datamond.crm.service;


import com.datamond.crm.entity.UserRole;

import java.util.List;

public interface UserRoleService {

    List<UserRole> getUserRoleListByRoleIdInUserId(Long userId);
}
