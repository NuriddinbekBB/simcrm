package com.datamond.crm.service;
import com.datamond.crm.entity.Branch;
import com.datamond.crm.entity.Region;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

public interface RegionService {
    DataTablesOutput<Region> getDeletedTrueRegionListInDataTableOutput(DataTablesInput input);
    Region findById(Long id);
    Boolean createOrUpdateByObject(Region region);
    void delete(Region region);
    
}
