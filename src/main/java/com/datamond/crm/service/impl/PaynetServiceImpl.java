package com.datamond.crm.service.impl;

import com.datamond.crm.dto.dashboard.PaynetDtoDebt;
import com.datamond.crm.dto.dashboard.PaynetDtoDebtPLan;
import com.datamond.crm.dto.dashboard.SimcardDtoForDashboard;
import com.datamond.crm.entity.Branch;
import com.datamond.crm.entity.Operator;
import com.datamond.crm.entity.Paynet;
import com.datamond.crm.repository.BranchRepository;
import com.datamond.crm.repository.PaynetRepository;
import com.datamond.crm.repository.UserRoleRepository;
import com.datamond.crm.service.BranchService;
import com.datamond.crm.service.PaketService;
import com.datamond.crm.service.PaynetService;
import com.datamond.crm.specification.DataTablesSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.DateUtils;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class PaynetServiceImpl implements PaynetService {

    @Autowired
    private PaynetRepository paynetRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;


    @Autowired
    private BranchService branchService;


    @Override
    public DataTablesOutput<Paynet> getDeletedTruePaynetListInDataTableOutput(DataTablesInput input) {
        return paynetRepository.findAll(input, DataTablesSpecification.allPaynet());
    }

    @Override
    public Paynet findById(Long id) {
        return paynetRepository.findById(id);
    }

    @Override
    public List<Paynet> findAll() {
        return paynetRepository.findAllByDeletedFalse();
    }

    @Override
    public Boolean createOrUpdateByObject(Paynet paynet) {
        if (paynet.exist()){
            if (paynet.getActive() == null) {
                paynet.setActive(Boolean.FALSE);
            } else {
                paynet.setActive(Boolean.TRUE);
            }
            Paynet old = paynetRepository.findById(paynet.getId());
            old.merge(paynet);
            paynet = old;
            return paynetRepository.saveAndFlush(paynet) != null;
        }else {
            // create
            return paynetRepository.saveAndFlush(paynet) != null;
        }
    }

    @Override
    public void delete(Paynet paynet) {
        paynet.setDeleted(Boolean.TRUE);
        paynet.setActive(Boolean.FALSE);
        paynetRepository.save(paynet);
    }

    @Override
    public List<PaynetDtoDebtPLan> getDataPaynet() {
        List<Branch> branches = branchService.findAll();
        List<PaynetDtoDebtPLan> sims = new ArrayList<>();
        for (Branch branch:branches){
            PaynetDtoDebt paynet = paynetRepository.getPaynetForDashboard(
                    branch.getId(),
                    DateUtils.createToday().getTime());
            sims.add(new PaynetDtoDebtPLan(branch.getBranchName(), paynet.getPlan(), paynet.getFact()));
        }
        return sims;
    }
}
