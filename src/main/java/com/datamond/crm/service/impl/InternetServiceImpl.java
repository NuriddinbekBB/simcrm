package com.datamond.crm.service.impl;

import com.datamond.crm.entity.Internet;
import com.datamond.crm.entity.Sms;
import com.datamond.crm.repository.BranchRepository;
import com.datamond.crm.repository.InternetRepository;
import com.datamond.crm.repository.SmsRepository;
import com.datamond.crm.repository.UserRoleRepository;
import com.datamond.crm.service.InternetService;
import com.datamond.crm.service.SmsService;
import com.datamond.crm.specification.DataTablesSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class InternetServiceImpl implements InternetService {

    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private InternetRepository internetRepository;

    @Override
    public DataTablesOutput<Internet> getDeletedTrueInternetListInDataTableOutput(DataTablesInput input) {
        return internetRepository.findAll(input, DataTablesSpecification.allInternet());
    }

    @Override
    public Internet findById(Long id) {
        return internetRepository.findById(id);
    }

    @Override
    public Boolean createOrUpdateByObject(Internet internet) {
        if (internet.exist()){
            if (internet.getActive() == null) {
                internet.setActive(Boolean.FALSE);
            } else {
                internet.setActive(Boolean.TRUE);
            }
            Internet old = internetRepository.findById(internet.getId());
            old.merge(internet);
            internet = old;
            return internetRepository.saveAndFlush(internet) != null;
        }else {
            // create
            return internetRepository.saveAndFlush(internet) != null;
        }
    }

    @Override
    public void delete(Internet internet) {
        internet.setDeleted(Boolean.TRUE);
        internet.setActive(Boolean.FALSE);
        internetRepository.save(internet);
    }
}
