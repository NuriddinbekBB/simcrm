package com.datamond.crm.service.impl;

import com.datamond.crm.entity.Paket;
import com.datamond.crm.entity.Sms;
import com.datamond.crm.repository.BranchRepository;
import com.datamond.crm.repository.PaketRepository;
import com.datamond.crm.repository.SmsRepository;
import com.datamond.crm.repository.UserRoleRepository;
import com.datamond.crm.service.PaketService;
import com.datamond.crm.service.SmsService;
import com.datamond.crm.specification.DataTablesSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SmsServiceImpl implements SmsService {

    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private SmsRepository smsRepository;

    @Override
    public DataTablesOutput<Sms> getDeletedTrueSmsListInDataTableOutput(DataTablesInput input) {
        return smsRepository.findAll(input, DataTablesSpecification.allSms());
    }

    @Override
    public Sms findById(Long id) {
        return smsRepository.findById(id);
    }

    @Override
    public Boolean createOrUpdateByObject(Sms sms) {
        if (sms.exist()){
            if (sms.getActive() == null) {
                sms.setActive(Boolean.FALSE);
            } else {
                sms.setActive(Boolean.TRUE);
            }
            Sms old = smsRepository.findById(sms.getId());
            old.merge(sms);
            sms = old;
            return smsRepository.saveAndFlush(sms) != null;
        }else {
            // create
            return smsRepository.saveAndFlush(sms) != null;
        }
    }

    @Override
    public void delete(Sms sms) {
        sms.setDeleted(Boolean.TRUE);
        sms.setActive(Boolean.FALSE);
        smsRepository.save(sms);
    }
}
