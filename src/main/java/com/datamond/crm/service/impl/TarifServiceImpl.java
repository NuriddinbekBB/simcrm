package com.datamond.crm.service.impl;

import com.datamond.crm.entity.Region;
import com.datamond.crm.entity.Tarif;
import com.datamond.crm.repository.BranchRepository;
import com.datamond.crm.repository.RegionRepository;
import com.datamond.crm.repository.TarifRepository;
import com.datamond.crm.repository.UserRoleRepository;
import com.datamond.crm.service.RegionService;
import com.datamond.crm.service.TarifService;
import com.datamond.crm.specification.DataTablesSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TarifServiceImpl implements TarifService {

    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private TarifRepository tarifRepository;

    @Override
    public DataTablesOutput<Tarif> getDeletedTrueTarifListInDataTableOutput(DataTablesInput input) {
        return tarifRepository.findAll(input, DataTablesSpecification.allTarif());
    }

    @Override
    public Tarif findById(Long id) {
        return tarifRepository.findById(id);
    }

    @Override
    public Boolean createOrUpdateByObject(Tarif tarif) {
        if (tarif.exist()){
            if (tarif.getActive() == null) {
                tarif.setActive(Boolean.FALSE);
            } else {
                tarif.setActive(Boolean.TRUE);
            }
            Tarif old = tarifRepository.findById(tarif.getId());
            old.merge(tarif);
            tarif = old;
            return tarifRepository.saveAndFlush(tarif) != null;
        }else {
            // create
            return tarifRepository.saveAndFlush(tarif) != null;
        }
    }

    @Override
    public void delete(Tarif tarif) {
        tarif.setDeleted(Boolean.TRUE);
        tarif.setActive(Boolean.FALSE);
        tarifRepository.save(tarif);
    }
}
