package com.datamond.crm.service.impl;

import com.datamond.crm.constant.Role;
import com.datamond.crm.dto.dashboard.SimcardDto;
import com.datamond.crm.dto.dashboard.SimcardDtoForDashboard;
import com.datamond.crm.entity.Operator;
import com.datamond.crm.entity.Simcard;
import com.datamond.crm.entity.User;
import com.datamond.crm.repository.SimcardRepository;
import com.datamond.crm.repository.UserRepository;
import com.datamond.crm.repository.UserRoleRepository;
import com.datamond.crm.service.OperatorService;
import com.datamond.crm.service.SimcardService;
import com.datamond.crm.specification.DataTablesSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.DateUtils;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class SimcardServiceImpl implements SimcardService {

    @Autowired
    private SimcardRepository simcardRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OperatorService operatorService;

    @Override
    public DataTablesOutput<Simcard> getDeletedTrueSimcardListInDataTableOutput(DataTablesInput input, Long userId) {
        Role role = userRoleRepository.findByUserId(userId).getRole();
        if (role==Role.BRANCH){
            User branchUser = userRepository.findById(userId).get();
            return simcardRepository.findAll(input, DataTablesSpecification.allSimcards(branchUser.getBranchId()));
        }
        return simcardRepository.findAll(input, DataTablesSpecification.allSimcard());
    }

    @Override
    public Simcard findById(Long id) {
        return simcardRepository.findById(id);
    }

    @Override
    public Boolean createOrUpdateByObject(Simcard simcard) {
        if (simcard.exist()){
            if (simcard.getActive() == null) {
                simcard.setActive(Boolean.FALSE);
            } else {
                simcard.setActive(Boolean.TRUE);
            }
            Simcard old = simcardRepository.findById(simcard.getId());
            old.merge(simcard);
            simcard = old;
            return simcardRepository.saveAndFlush(simcard) != null;
        }else {
            // create
            return simcardRepository.saveAndFlush(simcard) != null;
        }
    }

    @Override
    public void delete(Simcard simcard) {
        simcard.setDeleted(Boolean.TRUE);
        simcard.setActive(Boolean.FALSE);
        simcardRepository.save(simcard);
    }

    @Override
    public List<SimcardDtoForDashboard> getData(Long userId, Long branchId) {
        Role role = userRoleRepository.findByUserId(userId).getRole();
        List<Operator> operators = operatorService.findAll();
        List<SimcardDtoForDashboard> sims = new ArrayList<>();
        Integer count = 0;
        if (role==Role.BRANCH){
            for (Operator operator:operators){
                count = simcardRepository.countSimcardByDeletedFalseAndRegisteredAtAndBranchIdAndOperatorId(
                        DateUtils.createToday().getTime(),
                        operator.getId(),
                        branchId);
                sims.add(new SimcardDtoForDashboard(operator.getOperatorName(), count, operator.getColor()));
            }

            return sims;
        }else if (role==Role.ADMINISTRATOR){
            for (Operator operator:operators){
                count = simcardRepository.countSimcardByDeletedFalseAndRegisteredAtAndOperatorId(
                        DateUtils.createToday().getTime(),
                        operator.getId());
                sims.add(new SimcardDtoForDashboard(operator.getOperatorName(), count, operator.getColor()));
            }

            return sims;
        }

        return null;

    }

    @Override
    public List<SimcardDtoForDashboard> getDataForComing(Long userId, Long branchId) {

        Role role = userRoleRepository.findByUserId(userId).getRole();
        List<Operator> operators = operatorService.findAll();
        List<SimcardDtoForDashboard> sims = new ArrayList<>();
        Integer count = 0;

        if (role==Role.BRANCH){
            for (Operator operator:operators){
                count = simcardRepository.getDashboardSimcard(
                        branchId,
                        operator.getId());
                sims.add(new SimcardDtoForDashboard(operator.getOperatorName(), count, operator.getColor()));
            }
            return sims;
        }else if (role==Role.ADMINISTRATOR){
            for (Operator operator:operators){
                count = simcardRepository.getDashboardSimcardAdmin(
                        operator.getId());
                sims.add(new SimcardDtoForDashboard(operator.getOperatorName(), count, operator.getColor()));
            }
            return sims;
        }else{
            return null;
        }
    }
}
