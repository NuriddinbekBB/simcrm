package com.datamond.crm.service.impl;

import com.datamond.crm.constant.Role;
import com.datamond.crm.entity.UserRole;
import com.datamond.crm.repository.UserRoleRepository;
import com.datamond.crm.service.UserRoleService;
import com.datamond.crm.specification.UserRoleSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@Transactional
public class UserRoleServiceImpl implements UserRoleService {

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Override
    public List<UserRole> getUserRoleListByRoleIdInUserId(Long userId) {

        Role role = userRoleRepository.findByUserId(userId).getRole();

        if (role == Role.ADMINISTRATOR) {
            return userRoleRepository.findAll();
        }
        else if (role == Role.MANAGER){
            List<Role> roleList = new ArrayList<>(Arrays.asList(Role.MANAGER));
            return userRoleRepository.findAll(UserRoleSpecification.userRoleListByRoleList(roleList));
        } else if (role == Role.BRANCH){
            List<Role> roleList = new ArrayList<>(Arrays.asList(Role.BRANCH));
            return userRoleRepository.findAll(UserRoleSpecification.userRoleListByRoleList(roleList));
        }
        else {
            List<Role> roleList = new ArrayList<>(Arrays.asList(Role.MANAGER));
            return userRoleRepository.findAll(UserRoleSpecification.userRoleListByRoleList(roleList));
        }
    }

}
