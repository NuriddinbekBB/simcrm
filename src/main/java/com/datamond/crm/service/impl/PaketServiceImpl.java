package com.datamond.crm.service.impl;

import com.datamond.crm.entity.Paket;
import com.datamond.crm.entity.Tarif;
import com.datamond.crm.repository.BranchRepository;
import com.datamond.crm.repository.PaketRepository;
import com.datamond.crm.repository.TarifRepository;
import com.datamond.crm.repository.UserRoleRepository;
import com.datamond.crm.service.PaketService;
import com.datamond.crm.service.TarifService;
import com.datamond.crm.specification.DataTablesSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PaketServiceImpl implements PaketService {

    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private PaketRepository paketRepository;

    @Override
    public DataTablesOutput<Paket> getDeletedTruePaketListInDataTableOutput(DataTablesInput input) {
        return paketRepository.findAll(input, DataTablesSpecification.allPaket());
    }

    @Override
    public Paket findById(Long id) {
        return paketRepository.findById(id);
    }

    @Override
    public Boolean createOrUpdateByObject(Paket paket) {
        if (paket.exist()){
            if (paket.getActive() == null) {
                paket.setActive(Boolean.FALSE);
            } else {
                paket.setActive(Boolean.TRUE);
            }
            Paket old = paketRepository.findById(paket.getId());
            old.merge(paket);
            paket = old;
            return paketRepository.saveAndFlush(paket) != null;
        }else {
            // create
            return paketRepository.saveAndFlush(paket) != null;
        }
    }

    @Override
    public void delete(Paket paket) {
        paket.setDeleted(Boolean.TRUE);
        paket.setActive(Boolean.FALSE);
        paketRepository.save(paket);
    }
}
