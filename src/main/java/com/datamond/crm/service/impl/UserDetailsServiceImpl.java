package com.datamond.crm.service.impl;

import com.datamond.crm.entity.User;
import com.datamond.crm.repository.UserRepository;
import com.datamond.crm.utils.ProjectUserDetails;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service("userInfoService")
public class UserDetailsServiceImpl implements UserDetailsService {

    private final Logger logger = LogManager.getLogger(UserDetailsServiceImpl.class);

    @Autowired
    private UserRepository userRepository;


    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        System.out.println(" username : " + username);
        if (username == null || username.isEmpty()) {
            System.out.println(" Username cannot be empty ");
            throw new UsernameNotFoundException(" Username cannot be empty ");
        }
        User userentity = userRepository.findByUsernameAndFetchRolesEarly(username);
        if (userentity == null) {
            System.out.println(" User [" + username + "] not found ");
            throw new UsernameNotFoundException(" User [" + username + "] not found ");
        }
        if (userentity.getEnabled() != Boolean.TRUE) {
            System.out.println(" Your account have been blocked  ");
            throw new UsernameNotFoundException(" Your account have been blocked ");
        }

        ProjectUserDetails userDetails = new ProjectUserDetails(userentity);
        Date now = new Date();

        userRepository.save(userentity);
        return userDetails;
    }


}
