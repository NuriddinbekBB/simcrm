package com.datamond.crm.service.impl;

import com.datamond.crm.constant.Role;
import com.datamond.crm.dto.dashboard.SimcardDtoForDashboard;
import com.datamond.crm.dto.dashboard.SmsDtoForDashboard;
import com.datamond.crm.entity.Operator;
import com.datamond.crm.entity.SmsXizmat;
import com.datamond.crm.entity.User;
import com.datamond.crm.repository.SmsXizmatRepository;
import com.datamond.crm.repository.UserRepository;
import com.datamond.crm.repository.UserRoleRepository;
import com.datamond.crm.service.OperatorService;
import com.datamond.crm.service.SmsXizmatService;
import com.datamond.crm.specification.DataTablesSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.DateUtils;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class XizmatServiceImpl implements SmsXizmatService {

    @Autowired
    private SmsXizmatRepository serviceRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private OperatorService operatorService;

    @Autowired
    private UserRepository userRepository;

    @Override
    public DataTablesOutput<SmsXizmat> getDeletedTrueServiceListInDataTableOutput(DataTablesInput input, Long userId) {
        Role role = userRoleRepository.findByUserId(userId).getRole();
        if (role==Role.BRANCH){
            User branchUser = userRepository.findById(userId).get();
            return serviceRepository.findAll(input, DataTablesSpecification.allServices(branchUser.getBranchId()));
        }
        return serviceRepository.findAll(input, DataTablesSpecification.allService());
    }

    @Override
    public SmsXizmat findById(Long id) {
        return serviceRepository.findById(id);
    }

    @Override
    public Boolean createOrUpdateByObject(SmsXizmat service) {
        if (service.exist()){
            if (service.getActive() == null) {
                service.setActive(Boolean.FALSE);
            } else {
                service.setActive(Boolean.TRUE);
            }
            SmsXizmat old = serviceRepository.findById(service.getId());
            old.merge(service);
            service = old;
            return serviceRepository.saveAndFlush(service) != null;
        }else {
            // create
            return serviceRepository.saveAndFlush(service) != null;
        }
    }

    @Override
    public void delete(SmsXizmat service) {
        service.setDeleted(Boolean.TRUE);
        service.setActive(Boolean.FALSE);
        serviceRepository.save(service);
    }

    @Override
    public List<SmsDtoForDashboard> getData(Long userId, Long branchId) {
        Role role = userRoleRepository.findByUserId(userId).getRole();
        List<Operator> operators = operatorService.findAll();
        List<SmsDtoForDashboard> sims = new ArrayList<>();
        Integer count = 0;
        if (role==Role.BRANCH){
            for (Operator operator:operators){
                count = serviceRepository.countSmsXizmatByDeletedFalseAndEnabledAtAndBranchIdAndOperatorId(
                        DateUtils.createToday().getTime(),
                        operator.getId(),
                        branchId);
                sims.add(new SmsDtoForDashboard(operator.getOperatorName(), count, operator.getColor()));
            }

            return sims;
        }else if (role==Role.ADMINISTRATOR){
            for (Operator operator:operators){
                count = serviceRepository.countSmsXizmatByDeletedFalseAndEnabledAtAndOperatorId(
                        DateUtils.createToday().getTime(),
                        operator.getId());
                sims.add(new SmsDtoForDashboard(operator.getOperatorName(), count, operator.getColor()));
            }
            return sims;
        }

        return null;

    }

    @Override
    public List<SmsDtoForDashboard> getDataForComing(Long userId, Long branchId) {

        Role role = userRoleRepository.findByUserId(userId).getRole();
        List<Operator> operators = operatorService.findAll();
        List<SmsDtoForDashboard> sims = new ArrayList<>();
        Integer count = 0;

        if (role==Role.BRANCH){
            for (Operator operator:operators){
                count = serviceRepository.getDashboardService(
                        branchId,
                        operator.getId());
                sims.add(new SmsDtoForDashboard(operator.getOperatorName(), count, operator.getColor()));
            }
            return sims;
        }else if (role==Role.ADMINISTRATOR){
            for (Operator operator:operators){
                count = serviceRepository.getDashboardServiceAdmin(
                        operator.getId());
                sims.add(new SmsDtoForDashboard(operator.getOperatorName(), count, operator.getColor()));
            }
            return sims;
        }else{
            return null;
        }
    }
}
