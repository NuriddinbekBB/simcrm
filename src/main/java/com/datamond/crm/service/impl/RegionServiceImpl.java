package com.datamond.crm.service.impl;

import com.datamond.crm.entity.Region;
import com.datamond.crm.repository.BranchRepository;
import com.datamond.crm.repository.RegionRepository;
import com.datamond.crm.repository.UserRoleRepository;
import com.datamond.crm.service.RegionService;
import com.datamond.crm.specification.DataTablesSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RegionServiceImpl implements RegionService {

    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private RegionRepository regionRepository;

    @Override
    public DataTablesOutput<Region> getDeletedTrueRegionListInDataTableOutput(DataTablesInput input) {
        return regionRepository.findAll(input, DataTablesSpecification.allRegion());
    }

    @Override
    public Region findById(Long id) {
        return regionRepository.findById(id);
    }

    @Override
    public Boolean createOrUpdateByObject(Region region) {
        if (region.exist()){
            if (region.getActive() == null) {
                region.setActive(Boolean.FALSE);
            } else {
                region.setActive(Boolean.TRUE);
            }
            Region old = regionRepository.findById(region.getId());
            old.merge(region);
            region = old;
            return regionRepository.saveAndFlush(region) != null;
        }else {
            // create
            return regionRepository.saveAndFlush(region) != null;
        }
    }

    @Override
    public void delete(Region region) {
        region.setDeleted(Boolean.TRUE);
        region.setActive(Boolean.FALSE);
        regionRepository.save(region);
    }
}
