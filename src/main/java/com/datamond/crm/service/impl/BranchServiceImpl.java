package com.datamond.crm.service.impl;

import com.datamond.crm.constant.Role;
import com.datamond.crm.entity.Branch;
import com.datamond.crm.entity.Simcard;
import com.datamond.crm.repository.BranchRepository;
import com.datamond.crm.repository.SimcardRepository;
import com.datamond.crm.repository.UserRoleRepository;
import com.datamond.crm.service.BranchService;
import com.datamond.crm.service.SimcardService;
import com.datamond.crm.specification.DataTablesSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class BranchServiceImpl implements BranchService {

    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;


    @Override
    public DataTablesOutput<Branch> getDeletedTrueBranchListInDataTableOutput(DataTablesInput input) {
        return branchRepository.findAll(input, DataTablesSpecification.allBranch());
    }

    @Override
    public Branch findById(Long id) {
        return branchRepository.findById(id);
    }

    @Override
    public List<Branch> findAll() {
        return branchRepository.findAllByDeletedFalse();
    }

    @Override
    public Boolean createOrUpdateByObject(Branch branch) {
        if (branch.exist()){
            if (branch.getActive() == null) {
                branch.setActive(Boolean.FALSE);
            } else {
                branch.setActive(Boolean.TRUE);
            }
            Branch old = branchRepository.findById(branch.getId());
            old.merge(branch);
            branch = old;
            return branchRepository.saveAndFlush(branch) != null;
        }else {
            // create
            return branchRepository.saveAndFlush(branch) != null;
        }
    }

    @Override
    public void delete(Branch branch) {
        branch.setDeleted(Boolean.TRUE);
        branch.setActive(Boolean.FALSE);
        branchRepository.save(branch);
    }
}
