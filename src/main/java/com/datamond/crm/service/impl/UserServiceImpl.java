package com.datamond.crm.service.impl;

import com.datamond.crm.constant.Role;
import com.datamond.crm.dto.UserData;
import com.datamond.crm.entity.User;
import com.datamond.crm.entity.UserRole;
import com.datamond.crm.repository.UserRepository;
import com.datamond.crm.repository.UserRoleRepository;
import com.datamond.crm.service.UserRoleService;
import com.datamond.crm.service.UserService;
import com.datamond.crm.specification.DataTablesSpecification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private Logger logger = LogManager.getLogger(UserServiceImpl.class);
    private final UserRepository userRepository;
    private final UserRoleService userRoleService;
    private final UserRoleRepository userRoleRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, UserRoleService userRoleService, UserRoleRepository userRoleRepository) {
        this.userRepository = userRepository;
        this.userRoleService = userRoleService;
        this.userRoleRepository = userRoleRepository;
    }


    private Role getRole(String roleName) {
        for (Role role : Role.values()) {
            if (role.toString().equals(roleName)) {
                return role;
            }
        }

        return Role.MANAGER;
    }

    @Override
    public void delete(User user) {
        user.setEnabled(Boolean.FALSE);
        user.setDeleted(Boolean.TRUE);

        userRepository.save(user);
    }

    @Override
    public DataTablesOutput<User> getDeletedTrueUserListInDataTableOutput(DataTablesInput input) {
        return userRepository.findAll(input, DataTablesSpecification.allUser());
    }

    @Override
    public void register(UserData userData) throws Exception{
        //Let's check if user already registered with us
        if(checkIfUserExist(userData.getUsername())){
            throw new Exception("User already exists for this username");
        }
        User userEntity = new User();
        BeanUtils.copyProperties(userData, userEntity);
        encodePassword(userEntity, userData);
        userEntity.setRoles(new HashSet<>(userRoleRepository.findAll()));

        Role existUserRole = getRole(userData.getRole());
        System.out.println("existUserRole = " + existUserRole);

            // create
            User newUser = userRepository.saveAndFlush(userEntity);

                UserRole userRole = new UserRole();
                userRole.setRole(existUserRole);
                userRole.setEnabled(Boolean.TRUE);
                userRole.setUserEntity(newUser);
                userRoleRepository.save(userRole);
    }

    @Override
    public boolean checkIfUserExist(String username) {
        return userRepository.findByUsernameAndFetchRolesEarly(username) != null;
    }

    @Override
    public User findById(Long id) {
        return userRepository.findById(id).get();
    }

    @Override
    public Boolean createOrUpdateByObject(User user) {
        Role existUserRole = getRole(user.getRole());
        System.out.println("existUserRole = " + existUserRole);
        if (user.exist()) {
            // update
            User old = userRepository.findById(user.getId()).get();
            old.merge(user);
            user = old;

            UserRole newUserRole = userRoleRepository.findByUserId(user.getId());
            newUserRole.setRole(Role.valueOf(user.getRole()));
            userRoleRepository.saveAndFlush(newUserRole);

            User existUser = userRepository.saveAndFlush(user);
            System.out.println("existUser = " + existUser);
            if (existUser != null) {
                UserRole userRole = userRoleRepository.findByUserId(existUser.getId());

                if (userRole != null) {
                    if (existUserRole != userRole.getRole()) {
                        userRole.setRole(existUserRole);

                        return userRoleRepository.saveAndFlush(userRole) != null;
                    } else {
                        return Boolean.TRUE;
                    }
                } else {
                    userRole.setRole(existUserRole);
                    userRole.setEnabled(Boolean.TRUE);
                    userRole.setUserEntity(existUser);

                    return userRoleRepository.saveAndFlush(userRole) != null;
                }
            }

            return Boolean.FALSE;
        } else {
            // create
            User newUser = userRepository.saveAndFlush(user);

            if (newUser != null) {
                UserRole userRole = new UserRole();
                userRole.setRole(existUserRole);
                userRole.setEnabled(Boolean.TRUE);
                userRole.setUserEntity(newUser);

                return userRoleRepository.save(userRole) != null;
            }

            return Boolean.FALSE;
        }
    }

    private void encodePassword(User userEntity, UserData user){
        userEntity.setPassword(passwordEncoder.encode(user.getPassword()));
    }



}
