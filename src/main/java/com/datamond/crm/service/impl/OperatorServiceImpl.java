package com.datamond.crm.service.impl;

import com.datamond.crm.entity.Operator;
import com.datamond.crm.entity.Region;
import com.datamond.crm.repository.BranchRepository;
import com.datamond.crm.repository.OperatorRepository;
import com.datamond.crm.repository.RegionRepository;
import com.datamond.crm.repository.UserRoleRepository;
import com.datamond.crm.service.OperatorService;
import com.datamond.crm.service.RegionService;
import com.datamond.crm.specification.DataTablesSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class OperatorServiceImpl implements OperatorService {

    @Autowired
    private OperatorRepository operatorRepository;

    @Override
    public DataTablesOutput<Operator> getDeletedTrueOperatorListInDataTableOutput(DataTablesInput input) {
        return operatorRepository.findAll(input, DataTablesSpecification.allOperator());
    }

    @Override
    public Operator findById(Long id) {
        return operatorRepository.findById(id);
    }

    @Override
    public Boolean createOrUpdateByObject(Operator operator) {
        if (operator.exist()){
            if (operator.getActive() == null) {
                operator.setActive(Boolean.FALSE);
            } else {
                operator.setActive(Boolean.TRUE);
            }
            Operator old = operatorRepository.findById(operator.getId());
            old.merge(operator);
            operator = old;
            return operatorRepository.saveAndFlush(operator) != null;
        }else {
            // create
            return operatorRepository.saveAndFlush(operator) != null;
        }
    }

    @Override
    public void delete(Operator operator) {
        operator.setDeleted(Boolean.TRUE);
        operator.setActive(Boolean.FALSE);
        operatorRepository.save(operator);
    }

    @Override
    public List<Operator> findAll() {
        return operatorRepository.findAllByDeletedFalse();
    }
}
