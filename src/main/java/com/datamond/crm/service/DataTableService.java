package com.datamond.crm.service;


import com.datamond.crm.dto.DataTableDto;

public interface DataTableService {
    DataTableDto get(String language);
}
