package com.datamond.crm.service;
import com.datamond.crm.dto.dashboard.SimcardDtoForDashboard;
import com.datamond.crm.entity.Simcard;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

import java.util.List;

public interface SimcardService {
    DataTablesOutput<Simcard> getDeletedTrueSimcardListInDataTableOutput(DataTablesInput input, Long id);
    Simcard findById(Long id);
    Boolean createOrUpdateByObject(Simcard simcard);
    void delete(Simcard simcard);
    List<SimcardDtoForDashboard> getData(Long userId, Long branchId);
    List<SimcardDtoForDashboard> getDataForComing(Long userId, Long branchId);
}
