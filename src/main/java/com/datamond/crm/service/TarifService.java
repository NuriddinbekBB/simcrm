package com.datamond.crm.service;
import com.datamond.crm.entity.Tarif;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

public interface TarifService {
    DataTablesOutput<Tarif> getDeletedTrueTarifListInDataTableOutput(DataTablesInput input);
    Tarif findById(Long id);
    Boolean createOrUpdateByObject(Tarif tarif);
    void delete(Tarif tarif);
    
}
