package com.datamond.crm.service;

import com.datamond.crm.repository.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductService {

    @Autowired
    private ProductRepo productRepo;

    public void updateQuantityIProduct(int quantity,int id){
        productRepo.updateQuantity(quantity,id);
    }

}
