package com.datamond.crm.service;
import com.datamond.crm.entity.Branch;
import com.datamond.crm.entity.Simcard;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

import java.util.List;

public interface BranchService {
    DataTablesOutput<Branch> getDeletedTrueBranchListInDataTableOutput(DataTablesInput input);
    Branch findById(Long id);
    List<Branch> findAll();
    Boolean createOrUpdateByObject(Branch branch);
    void delete(Branch branch);
    
}
