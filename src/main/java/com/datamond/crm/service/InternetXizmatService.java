package com.datamond.crm.service;
import com.datamond.crm.dto.dashboard.InternetDtoForDashboard;
import com.datamond.crm.dto.dashboard.SmsDtoForDashboard;
import com.datamond.crm.entity.InternetXizmat;
import com.datamond.crm.entity.SmsXizmat;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

import java.util.List;

public interface InternetXizmatService {
    DataTablesOutput<InternetXizmat> getDeletedTrueXizmatListInDataTableOutput(DataTablesInput input, Long id);
    InternetXizmat findById(Long id);
    Boolean createOrUpdateByObject(InternetXizmat service);
    void delete(InternetXizmat service);


    List<InternetDtoForDashboard> getData(Long userId, Long branchId);

    List<InternetDtoForDashboard> getDataForComing(Long userId, Long branchId);
}
