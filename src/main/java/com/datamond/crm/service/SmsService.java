package com.datamond.crm.service;
import com.datamond.crm.entity.Paket;
import com.datamond.crm.entity.Sms;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

public interface SmsService {
    DataTablesOutput<Sms> getDeletedTrueSmsListInDataTableOutput(DataTablesInput input);
    Sms findById(Long id);
    Boolean createOrUpdateByObject(Sms sms);
    void delete(Sms sms);
}