package com.datamond.crm.constant;

public class Templates {

    // upload file
    public static final String UploadFiles              = "/upload_files";

    public static final String Login                    = "/login";
    public static final String Register                    = "/register";
    public static final String Logout                   = "/logout";

    public static final String ErrorPage403             = "/403";
    public static final String ErrorPage404             = "/404";
    public static final String ErrorPage500             = "/500";

    //  =========================  ADMINISTRATOR  ========================================================================
    public static final String Admin                    = "/admin";
    // Users
    public static final String AdminUsers               = Admin + "/user";
    public static final String AdminUsersList           = AdminUsers + "/list";
    public static final String AdminUsersListDataTable  = AdminUsers + "/list_data_table";
    public static final String AdminUsersView           = AdminUsers + "/view";
    public static final String AdminUsersCreate         = AdminUsers + "/create";
    public static final String AdminUsersEdit           = AdminUsers + "/edit";
    public static final String AdminUsersSave           = AdminUsers + "/save";
    public static final String AdminUsersDelete         = AdminUsers + "/delete";
    public static final String AdminUsersProfile         = AdminUsers + "/profile";
    public static final String AdminDashboard           = Admin + "/dashboard";


    // Model
    public static final String Model                      = "/model";
    public static final String ModelDashboard             = Model + "/dashboard";

    // Simcard
    public static final String ModelSimcard            = Model + "/simcard";
    public static final String ModelSimcardList            = ModelSimcard + "/list";
    public static final String ModelSimcardEdit            = ModelSimcard + "/edit";
    public static final String ModelSimcardCreate            = ModelSimcard + "/create";
    public static final String ModelSimcardSave            = ModelSimcard + "/save";
    public static final String ModelSimcardCancel            = ModelSimcard + "/cancel";
    public static final String ModelSimcardView            = ModelSimcard + "/view";
    public static final String ModelSimcardAjax            = ModelSimcard + "/list_data_table";
    public static final String ModelSimcardAjaxByView            = ModelSimcard + "/list_data_table_view";
    public static final String ModelSimcardDelete            = ModelSimcard + "/delete";


    // Paynet
    public static final String ModelPaynet            = Model + "/paynet";
    public static final String ModelPaynetList            = ModelPaynet + "/list";
    public static final String ModelPaynetEdit            = ModelPaynet + "/edit";
    public static final String ModelPaynetCreate            = ModelPaynet + "/create";
    public static final String ModelPaynetSave            = ModelPaynet + "/save";
    public static final String ModelPaynetCancel            = ModelPaynet + "/cancel";
    public static final String ModelPaynetView            = ModelPaynet + "/view";
    public static final String ModelPaynetListDataTable          = ModelPaynet + "/list_data_table";
    public static final String ModelPaynetAjaxByView            = ModelPaynet + "/list_data_table_view";
    public static final String ModelPaynetDelete            = ModelPaynet + "/delete";

    // Service
    public static final String ModelService           = Model + "/service";
    public static final String ModelServiceList            = ModelService + "/list";
    public static final String ModelServiceEdit            = ModelService + "/edit";
    public static final String ModelServiceCreate            = ModelService + "/create";
    public static final String ModelServiceSave            = ModelService + "/save";
    public static final String ModelServiceCancel            = ModelService + "/cancel";
    public static final String ModelServiceView            = ModelService + "/view";
    public static final String ModelServiceListDataTable          = ModelService + "/list_data_table";
    public static final String ModelServiceAjaxByView            = ModelService + "/list_data_table_view";
    public static final String ModelServiceDelete            = ModelService + "/delete";


    // Branch
    public static final String ModelBranch            = Model + "/branch";
    public static final String ModelBranchList            = ModelBranch + "/list";
    public static final String ModelBranchEdit            = ModelBranch + "/edit";
    public static final String ModelBranchCreate            = ModelBranch + "/create";
    public static final String ModelBranchSave            = ModelBranch + "/save";
    public static final String ModelBranchCancel            = ModelBranch + "/cancel";
    public static final String ModelBranchView            = ModelBranch + "/view";
    public static final String ModelBranchListDataTable          = ModelBranch + "/list_data_table";
    public static final String ModelBranchAjaxByView            = ModelBranch + "/list_data_table_view";
    public static final String ModelBranchDelete            = ModelBranch + "/delete";

    // Region
    public static final String ModelRegion            = Model + "/region";
    public static final String ModelRegionList            = ModelRegion + "/list";
    public static final String ModelRegionEdit            = ModelRegion + "/edit";
    public static final String ModelRegionCreate            = ModelRegion + "/create";
    public static final String ModelRegionSave            = ModelRegion + "/save";
    public static final String ModelRegionCancel            = ModelRegion + "/cancel";
    public static final String ModelRegionView            = ModelRegion + "/view";
    public static final String ModelRegionListDataTable          = ModelRegion + "/list_data_table";
    public static final String ModelRegionAjaxByView            = ModelRegion + "/list_data_table_view";
    public static final String ModelRegionDelete            = ModelRegion + "/delete";


    // Operator
    public static final String ModelOperator            = Model + "/oper";
    public static final String ModelOperatorList            = ModelOperator + "/list";
    public static final String ModelOperatorEdit            = ModelOperator + "/edit";
    public static final String ModelOperatorCreate            = ModelOperator + "/create";
    public static final String ModelOperatorSave            = ModelOperator + "/save";
    public static final String ModelOperatorCancel            = ModelOperator + "/cancel";
    public static final String ModelOperatorView            = ModelOperator + "/view";
    public static final String ModelOperatorListDataTable          = ModelOperator + "/list_data_table";
    public static final String ModelOperatorAjaxByView            = ModelOperator + "/list_data_table_view";
    public static final String ModelOperatorDelete            = ModelOperator + "/delete";

    // Tarif
    public static final String ModelTarif            = Model + "/tarif";
    public static final String ModelTarifList            = ModelTarif + "/list";
    public static final String ModelTarifEdit            = ModelTarif + "/edit";
    public static final String ModelTarifCreate            = ModelTarif + "/create";
    public static final String ModelTarifSave            = ModelTarif + "/save";
    public static final String ModelTarifCancel            = ModelTarif + "/cancel";
    public static final String ModelTarifView            = ModelTarif + "/view";
    public static final String ModelTarifListDataTable          = ModelTarif + "/list_data_table";
    public static final String ModelTarifAjaxByView            = ModelTarif + "/list_data_table_view";
    public static final String ModelTarifDelete            = ModelTarif + "/delete";

    // Paket
    public static final String ModelPaket            = Model + "/paket";
    public static final String ModelPaketList            = ModelPaket + "/list";
    public static final String ModelPaketEdit            = ModelPaket + "/edit";
    public static final String ModelPaketCreate            = ModelPaket + "/create";
    public static final String ModelPaketSave            = ModelPaket + "/save";
    public static final String ModelPaketCancel            = ModelPaket + "/cancel";
    public static final String ModelPaketView            = ModelPaket + "/view";
    public static final String ModelPaketListDataTable          = ModelPaket + "/list_data_table";
    public static final String ModelPaketAjaxByView            = ModelPaket + "/list_data_table_view";
    public static final String ModelPaketDelete            = ModelPaket + "/delete";

    // Sms
    public static final String ModelSms            = Model + "/sms";
    public static final String ModelSmsList            = ModelSms + "/list";
    public static final String ModelSmsEdit            = ModelSms + "/edit";
    public static final String ModelSmsCreate            = ModelSms + "/create";
    public static final String ModelSmsSave            = ModelSms + "/save";
    public static final String ModelSmsCancel            = ModelSms + "/cancel";
    public static final String ModelSmsView            = ModelSms + "/view";
    public static final String ModelSmsListDataTable          = ModelSms + "/list_data_table";
    public static final String ModelSmsAjaxByView            = ModelSms + "/list_data_table_view";
    public static final String ModelSmsDelete            = ModelSms + "/delete";


    // Internet
    public static final String ModelInternet            = Model + "/internet";
    public static final String ModelInternetList            = ModelInternet + "/list";
    public static final String ModelInternetEdit            = ModelInternet + "/edit";
    public static final String ModelInternetCreate            = ModelInternet + "/create";
    public static final String ModelInternetSave            = ModelInternet + "/save";
    public static final String ModelInternetCancel            = ModelInternet + "/cancel";
    public static final String ModelInternetView            = ModelInternet + "/view";
    public static final String ModelInternetListDataTable          = ModelInternet + "/list_data_table";
    public static final String ModelInternetAjaxByView            = ModelInternet + "/list_data_table_view";
    public static final String ModelInternetDelete            = ModelInternet + "/delete";



    // Internet Service
    public static final String ModelInterService           = Model + "/iservice";
    public static final String ModelInterServiceList            = ModelInterService + "/list";
    public static final String ModelInterServiceEdit            = ModelInterService + "/edit";
    public static final String ModelInterServiceCreate            = ModelInterService + "/create";
    public static final String ModelInterServiceSave            = ModelInterService + "/save";
    public static final String ModelInterServiceCancel            = ModelInterService + "/cancel";
    public static final String ModelInterServiceView            = ModelInterService + "/view";
    public static final String ModelInterServiceListDataTable          = ModelInterService + "/list_data_table";
    public static final String ModelInterServiceAjaxByView            = ModelInterService + "/list_data_table_view";
    public static final String ModelInterServiceDelete            = ModelInterService + "/delete";


}
