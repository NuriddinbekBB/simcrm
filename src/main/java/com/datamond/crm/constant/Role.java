package com.datamond.crm.constant;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public enum Role {
    ROLE_ANONYMOUS(null),
    ADMINISTRATOR(null),
    MANAGER(ADMINISTRATOR),
    BRANCH(MANAGER);

    private Role parent;
    private Set<Role> children = new HashSet<>();


    Role(Role parent) {
        this.parent = parent;
        if (this.parent != null) {
            this.parent.addChild(this.parent);
            this.parent.addChild(this);
        }
    }
    private static void addChildren(Role root, Set<Role> roles) {
        roles.addAll(root.children);
        root.children.forEach(role -> roles.addAll(role.children));
    }
    public Role parent() {
        return parent;
    }
    public boolean is(Role other) {
        if (other == null) {
            return false;
        }

        for (Role role = this; role != null; role = role.parent()) {
            if (other == role) {
                return true;
            }
        }
        return false;
    }
    public Role[] children() {
        return children.toArray(new Role[children.size()]);
    }

    public Role[] allChildren() {
        Set<Role> set = new HashSet<>();
        set.add(this);
        addChildren(this, set);
        Role[] roles = set.toArray(new Role[set.size()]);
        Arrays.sort(roles);
        return roles;
    }

    private void addChild(Role child) {
        if (!this.children.contains(child))
            this.children.add(child);
    }
}