package com.datamond.crm.constant;

import java.text.SimpleDateFormat;

public class Properties {
    public static final String uploadPath = "upload_files";
    public static final String UrlRedirect   = "redirect:";
    public static final SimpleDateFormat uzbekistanDateFormat = new SimpleDateFormat("dd-MM-yyyy");
}
