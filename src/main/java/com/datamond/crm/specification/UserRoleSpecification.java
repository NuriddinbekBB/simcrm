package com.datamond.crm.specification;

import com.datamond.crm.constant.Role;
import com.datamond.crm.entity.UserRole;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.LinkedList;
import java.util.List;

public class UserRoleSpecification {
    public static Specification<UserRole> userRoleListByRoleList(List<Role> roleList) {

        return (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new LinkedList<>();
            predicates.add(criteriaBuilder.equal(root.get("enabled"), Boolean.TRUE));
            predicates.add(root.get("role").in(roleList));

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
