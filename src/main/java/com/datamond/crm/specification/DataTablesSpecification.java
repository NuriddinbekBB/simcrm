package com.datamond.crm.specification;


import com.datamond.crm.entity.*;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.LinkedList;
import java.util.List;

public class DataTablesSpecification {

    public DataTablesSpecification() {

    }

    public static Specification<Simcard> allSimcard() {
        return new Specification<Simcard>() {
            @Override
            public Predicate toPredicate(Root<Simcard> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new LinkedList<>();
                predicates.add(criteriaBuilder.equal(root.get("deleted"), Boolean.FALSE));
                Predicate overAll = criteriaBuilder.and(predicates.toArray(new Predicate[0]));
                return overAll;
            }
        };
    }
    public static Specification<SmsXizmat> allService() {
        return new Specification<SmsXizmat>() {
            @Override
            public Predicate toPredicate(Root<SmsXizmat> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new LinkedList<>();
                predicates.add(criteriaBuilder.equal(root.get("deleted"), Boolean.FALSE));
                Predicate overAll = criteriaBuilder.and(predicates.toArray(new Predicate[0]));
                return overAll;
            }
        };
    }
    public static Specification<InternetXizmat> allInternetService() {
        return new Specification<InternetXizmat>() {
            @Override
            public Predicate toPredicate(Root<InternetXizmat> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new LinkedList<>();
                predicates.add(criteriaBuilder.equal(root.get("deleted"), Boolean.FALSE));
                Predicate overAll = criteriaBuilder.and(predicates.toArray(new Predicate[0]));
                return overAll;
            }
        };
    }
    public static Specification<Simcard> allSimcards(Long ids) {
        return new Specification<Simcard>() {
            @Override
            public Predicate toPredicate(Root<Simcard> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new LinkedList<>();
                predicates.add(criteriaBuilder.equal(root.get("deleted"), Boolean.FALSE));
                predicates.add(criteriaBuilder.equal(root.get("branchId"), ids));
                Predicate overAll = criteriaBuilder.and(predicates.toArray(new Predicate[0]));
                return overAll;
            }
        };
    }

    public static Specification<SmsXizmat> allServices(Long ids) {
        return new Specification<SmsXizmat>() {
            @Override
            public Predicate toPredicate(Root<SmsXizmat> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new LinkedList<>();
                predicates.add(criteriaBuilder.equal(root.get("deleted"), Boolean.FALSE));
                predicates.add(criteriaBuilder.equal(root.get("branchId"), ids));
                Predicate overAll = criteriaBuilder.and(predicates.toArray(new Predicate[0]));
                return overAll;
            }
        };
    }
    public static Specification<InternetXizmat> allInternetServices(Long ids) {
        return new Specification<InternetXizmat>() {
            @Override
            public Predicate toPredicate(Root<InternetXizmat> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new LinkedList<>();
                predicates.add(criteriaBuilder.equal(root.get("deleted"), Boolean.FALSE));
                predicates.add(criteriaBuilder.equal(root.get("branchId"), ids));
                Predicate overAll = criteriaBuilder.and(predicates.toArray(new Predicate[0]));
                return overAll;
            }
        };
    }

    public static Specification<Branch> allBranch() {
        return new Specification<Branch>() {
            @Override
            public Predicate toPredicate(Root<Branch> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new LinkedList<>();
                predicates.add(criteriaBuilder.equal(root.get("deleted"), Boolean.FALSE));
                Predicate overAll = criteriaBuilder.and(predicates.toArray(new Predicate[0]));
                return overAll;
            }
        };
    }
    public static Specification<Paynet> allPaynet() {
        return new Specification<Paynet>() {
            @Override
            public Predicate toPredicate(Root<Paynet> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new LinkedList<>();
                predicates.add(criteriaBuilder.equal(root.get("deleted"), Boolean.FALSE));
                Predicate overAll = criteriaBuilder.and(predicates.toArray(new Predicate[0]));
                return overAll;
            }
        };
    }
    public static Specification<Region> allRegion() {
        return new Specification<Region>() {
            @Override
            public Predicate toPredicate(Root<Region> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new LinkedList<>();
                predicates.add(criteriaBuilder.equal(root.get("deleted"), Boolean.FALSE));
                Predicate overAll = criteriaBuilder.and(predicates.toArray(new Predicate[0]));
                return overAll;
            }
        };
    }

    public static Specification<Tarif> allTarif() {
        return new Specification<Tarif>() {
            @Override
            public Predicate toPredicate(Root<Tarif> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new LinkedList<>();
                predicates.add(criteriaBuilder.equal(root.get("deleted"), Boolean.FALSE));
                Predicate overAll = criteriaBuilder.and(predicates.toArray(new Predicate[0]));
                return overAll;
            }
        };
    }

    public static Specification<Paket> allPaket() {
        return new Specification<Paket>() {
            @Override
            public Predicate toPredicate(Root<Paket> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new LinkedList<>();
                predicates.add(criteriaBuilder.equal(root.get("deleted"), Boolean.FALSE));
                Predicate overAll = criteriaBuilder.and(predicates.toArray(new Predicate[0]));
                return overAll;
            }
        };
    }

    public static Specification<Sms> allSms() {
        return new Specification<Sms>() {
            @Override
            public Predicate toPredicate(Root<Sms> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new LinkedList<>();
                predicates.add(criteriaBuilder.equal(root.get("deleted"), Boolean.FALSE));
                Predicate overAll = criteriaBuilder.and(predicates.toArray(new Predicate[0]));
                return overAll;
            }
        };
    }

    public static Specification<Internet> allInternet() {
        return new Specification<Internet>() {
            @Override
            public Predicate toPredicate(Root<Internet> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new LinkedList<>();
                predicates.add(criteriaBuilder.equal(root.get("deleted"), Boolean.FALSE));
                Predicate overAll = criteriaBuilder.and(predicates.toArray(new Predicate[0]));
                return overAll;
            }
        };
    }

    public static Specification<Operator> allOperator() {
        return new Specification<Operator>() {
            @Override
            public Predicate toPredicate(Root<Operator> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new LinkedList<>();
                predicates.add(criteriaBuilder.equal(root.get("deleted"), Boolean.FALSE));
                Predicate overAll = criteriaBuilder.and(predicates.toArray(new Predicate[0]));
                return overAll;
            }
        };
    }

    public static Specification<User> allUser() {
        return new Specification<User>() {
            @Override
            public Predicate toPredicate(Root<User> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new LinkedList<>();
                predicates.add(criteriaBuilder.equal(root.get("deleted"), Boolean.FALSE));
                Predicate overAll = criteriaBuilder.and(predicates.toArray(new Predicate[0]));
                return overAll;
            }
        };
    }
}
