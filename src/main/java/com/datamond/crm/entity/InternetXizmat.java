package com.datamond.crm.entity;

import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Entity
@Table(name = "iservice")
//@EntityListeners(AuditEntityListener.class)
public class InternetXizmat implements Serializable {
    @Transient
    private static final String sequenceName = "iservice_id_seq";

    @Id
    @SequenceGenerator(name = sequenceName, sequenceName = sequenceName, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = sequenceName)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "branch_id", insertable = false, updatable = false)
    @Fetch(FetchMode.JOIN)
    private Branch branch;

    @Column(name = "branch_id")
    private Long branchId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "internet_id", insertable = false, updatable = false)
    @Fetch(FetchMode.JOIN)
    private Internet internet;

    @Column(name = "internet_id")
    private Long internetId;

    @JsonView(DataTablesOutput.View.class)
    @Column(name = "abonent_first_name")
    private String abonentFirstName;

    @JsonView(DataTablesOutput.View.class)
    @Column(name = "abonent_last_name")
    private String abonentLastName;

    @JsonView(DataTablesOutput.View.class)
    @Column(name = "phonenumber")
    private String phonenumber;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "operator_id", insertable = false, updatable = false)
    @Fetch(FetchMode.JOIN)
    private Operator operator;

    @Column(name = "operator_id")
    private Long operatorId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "region_id", insertable = false, updatable = false)
    @Fetch(FetchMode.JOIN)
    private Region region;

    @Column(name = "region_id")
    private Long regionId;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonView(DataTablesOutput.View.class)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date enabledAt;

    @JsonView(DataTablesOutput.View.class)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthdate;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonView(DataTablesOutput.View.class)
    private Date registeredAt;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonView(DataTablesOutput.View.class)
    private Date updatedAt;

    private Boolean deleted;

    private Boolean active;

    @PrePersist
    public void onCreate() {
        setRegisteredAt(new Date());
        setUpdatedAt(new Date());
        setDeleted(Boolean.FALSE);
        setActive(Boolean.TRUE);
    }

    @PreUpdate
    public void onUpdate() {
        setUpdatedAt(new Date());
    }


    public void merge(InternetXizmat other) {
        this.id = other.id != null ? other.id : this.id;
        this.deleted = other.deleted != null ? other.deleted : this.deleted;
        this.active = other.active != null ? other.active : this.active;
        this.registeredAt = other.registeredAt != null ? other.registeredAt : this.registeredAt;
        this.updatedAt = other.updatedAt != null ? other.updatedAt : this.updatedAt;
        this.abonentFirstName = other.abonentFirstName != null ? other.abonentFirstName : this.abonentFirstName;
        this.abonentLastName = other.abonentLastName != null ? other.abonentLastName : this.abonentLastName;
        this.operator = other.operator != null ? other.operator : this.operator;
        this.operatorId = other.operatorId != null ? other.operatorId : this.operatorId;
        this.birthdate = other.birthdate != null ? other.birthdate : this.birthdate;
        this.branch = other.branch != null ? other.branch : this.branch;
        this.branchId = other.branchId != null ? other.branchId : this.branchId;
        this.internet = other.internet != null ? other.internet : this.internet;
        this.internetId = other.internetId != null ? other.internetId : this.internetId;
        this.region = other.region != null ? other.region : this.region;
        this.regionId = other.regionId != null ? other.regionId : this.regionId;
        this.enabledAt = other.enabledAt != null ? other.enabledAt : this.enabledAt;
        this.phonenumber = other.phonenumber != null ? other.phonenumber : this.phonenumber;
    }

    public boolean exist() {
        return this.id != null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    public String getAbonentFirstName() {
        return abonentFirstName;
    }

    public void setAbonentFirstName(String abonentFirstName) {
        this.abonentFirstName = abonentFirstName;
    }

    public String getAbonentLastName() {
        return abonentLastName;
    }

    public void setAbonentLastName(String abonentLastName) {
        this.abonentLastName = abonentLastName;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }

    public Date getEnabledAt() {
        return enabledAt;
    }

    public void setEnabledAt(Date enabledAt) {
        this.enabledAt = enabledAt;
    }

    public Date getRegisteredAt() {
        return registeredAt;
    }

    public void setRegisteredAt(Date registeredAt) {
        this.registeredAt = registeredAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    public Internet getInternet() {
        return internet;
    }

    public void setInternet(Internet internet) {
        this.internet = internet;
    }

    public Long getInternetId() {
        return internetId;
    }

    public void setInternetId(Long internetId) {
        this.internetId = internetId;
    }

    public InternetXizmat() {
    }

    public InternetXizmat(Long branchId) {
        this.branchId = branchId;
    }
}
