package com.datamond.crm.entity;

import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Entity
@Table(name = "region")
public class Region implements Serializable {
    @Transient
    private static final String sequenceName = "region_id_seq";

    @Id
    @SequenceGenerator(name = sequenceName, sequenceName = sequenceName, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = sequenceName)
    private Long id;

    @JsonView(DataTablesOutput.View.class)
    @Column(name = "region_name")
    private String regionName;

    @JsonView(DataTablesOutput.View.class)
    @Column(name = "lattitude")
    private Float lattitude;

    @JsonView(DataTablesOutput.View.class)
    @Column(name = "longtitude")
    private Float longtitude;

    @JsonView(DataTablesOutput.View.class)
    @Column(name = "info")
    private String info;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonView(DataTablesOutput.View.class)
    private Date registeredAt;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonView(DataTablesOutput.View.class)
    private Date updatedAt;

    private Boolean deleted;

    private Boolean active;

    @PrePersist
    public void onCreate() {
        setRegisteredAt(new Date());
        setUpdatedAt(new Date());
        setDeleted(Boolean.FALSE);
        setActive(Boolean.TRUE);
    }
    @PreUpdate
    public void onUpdate() {
        setUpdatedAt(new Date());
    }

    public void merge(Region otherRegion) {
        this.id = otherRegion.id != null ? otherRegion.id : this.id;
        this.deleted = otherRegion.deleted != null ? otherRegion.deleted : this.deleted;
        this.active = otherRegion.active != null ? otherRegion.active : this.active;
        this.regionName = otherRegion.regionName != null ? otherRegion.regionName : this.regionName;
        this.updatedAt = otherRegion.updatedAt != null ? otherRegion.updatedAt : this.updatedAt;
        this.lattitude = otherRegion.lattitude != null ? otherRegion.lattitude : this.lattitude;
        this.longtitude = otherRegion.longtitude != null ? otherRegion.longtitude : this.longtitude;
        this.info = otherRegion.info != null ? otherRegion.info : this.info;
        this.registeredAt = otherRegion.registeredAt != null ? otherRegion.registeredAt : this.registeredAt;
    }

    public boolean exist() {
        return this.id != null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getRegisteredAt() {
        return registeredAt;
    }

    public void setRegisteredAt(Date registeredAt) {
        this.registeredAt = registeredAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public Float getLattitude() {
        return lattitude;
    }

    public void setLattitude(Float lattitude) {
        this.lattitude = lattitude;
    }

    public Float getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(Float longtitude) {
        this.longtitude = longtitude;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
