package com.datamond.crm.entity;

import com.datamond.crm.constant.Role;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Entity
@Table(name = "user_roles")
public class UserRole implements Serializable{

    @Transient
    public static final String sequenceName = "user_roles_id_seq";

    @Id
    @SequenceGenerator(name = sequenceName, sequenceName = sequenceName, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = sequenceName)
    private Long id;

    @Enumerated(EnumType.STRING)
    private Role role;

    private Boolean enabled;

    @ManyToOne
    @JoinColumn
    private com.datamond.crm.entity.User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public com.datamond.crm.entity.User getUserEntity() {
        return user;
    }

    public void setUserEntity(com.datamond.crm.entity.User userEntity) {
        this.user = userEntity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserRole userRole = (UserRole) o;

        if (!id.equals(userRole.id)) return false;
        if (role != userRole.role) return false;
        return enabled != null ? enabled.equals(userRole.enabled) : userRole.enabled == null;

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + role.hashCode();
        result = 31 * result + (enabled != null ? enabled.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "UserRole{" +
                "id=" + id +
                ", role=" + role +
                ", enabled=" + enabled +
                '}';
    }
}
