package com.datamond.crm.entity;

import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Entity
@Table(name = "sms")
public class Sms implements Serializable {
    @Transient
    private static final String sequenceName = "sms_id_seq";

    @Id
    @SequenceGenerator(name = sequenceName, sequenceName = sequenceName, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = sequenceName)
    private Long id;

    @JsonView(DataTablesOutput.View.class)
    @Column(name = "paket_name")
    private String paketName;

    @JsonView(DataTablesOutput.View.class)
    @Column(name = "paket_cost")
    private String paketCost;

    @JsonView(DataTablesOutput.View.class)
    @Column(name = "duration")
    private Integer duration;

    @JsonView(DataTablesOutput.View.class)
    @Column(name = "info")
    private String info;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "operator_id", insertable = false, updatable = false)
    @Fetch(FetchMode.JOIN)
    private Operator operator;

    @Column(name = "operator_id")
    private Long operatorId;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonView(DataTablesOutput.View.class)
    private Date registeredAt;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonView(DataTablesOutput.View.class)
    private Date updatedAt;

    private Boolean deleted;

    private Boolean active;

    @PrePersist
    public void onCreate() {
        setRegisteredAt(new Date());
        setUpdatedAt(new Date());
        setDeleted(Boolean.FALSE);
        setActive(Boolean.TRUE);
    }
    @PreUpdate
    public void onUpdate() {
        setUpdatedAt(new Date());
    }

    public void merge(Sms otherRegion) {
        this.id = otherRegion.id != null ? otherRegion.id : this.id;
        this.deleted = otherRegion.deleted != null ? otherRegion.deleted : this.deleted;
        this.active = otherRegion.active != null ? otherRegion.active : this.active;
        this.paketName = otherRegion.paketName != null ? otherRegion.paketName : this.paketName;
        this.updatedAt = otherRegion.updatedAt != null ? otherRegion.updatedAt : this.updatedAt;
        this.paketCost = otherRegion.paketCost != null ? otherRegion.paketCost : this.paketCost;
        this.duration = otherRegion.duration != null ? otherRegion.duration : this.duration;
        this.operator = otherRegion.operator != null ? otherRegion.operator : this.operator;
        this.operatorId = otherRegion.operatorId != null ? otherRegion.operatorId : this.operatorId;
        this.info = otherRegion.info != null ? otherRegion.info : this.info;
        this.registeredAt = otherRegion.registeredAt != null ? otherRegion.registeredAt : this.registeredAt;
    }

    public boolean exist() {
        return this.id != null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getRegisteredAt() {
        return registeredAt;
    }

    public void setRegisteredAt(Date registeredAt) {
        this.registeredAt = registeredAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getPaketName() {
        return paketName;
    }

    public void setPaketName(String paketName) {
        this.paketName = paketName;
    }

    public String getPaketCost() {
        return paketCost;
    }

    public void setPaketCost(String paketCost) {
        this.paketCost = paketCost;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

}
