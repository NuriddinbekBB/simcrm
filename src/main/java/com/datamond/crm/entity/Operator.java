package com.datamond.crm.entity;

import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Entity
@Table(name = "operator")
public class Operator implements Serializable {
    @Transient
    private static final String sequenceName = "operator_id_seq";

    @Id
    @SequenceGenerator(name = sequenceName, sequenceName = sequenceName, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = sequenceName)
    private Long id;

    @JsonView(DataTablesOutput.View.class)
    @Column(name = "operator_name", length = 1024)
    private String operatorName;

    @JsonView(DataTablesOutput.View.class)
    @Column(name = "logo_url", length = 1024)
    private String logoUrl;

    @JsonView(DataTablesOutput.View.class)
    @Column(name = "info", length = 1024)
    private String info;

    @JsonView(DataTablesOutput.View.class)
    @Column(name = "color", length = 1024)
    private String color;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonView(DataTablesOutput.View.class)
    private Date createdAt;

    private Boolean deleted;

    private Boolean active;

    @PrePersist
    public void onCreate() {
        setCreatedAt(new Date());
        setDeleted(Boolean.FALSE);
        setActive(Boolean.TRUE);
    }
    public void merge(Operator other) {

        this.id = other.id != null ? other.id : this.id;
        this.operatorName = other.operatorName != null ? other.operatorName : this.operatorName;
        this.logoUrl = other.logoUrl != null ? other.logoUrl : this.logoUrl;
        this.active = other.active != null ? other.active : this.active;
        this.info = other.info != null ? other.info : this.info;
        this.color = other.color != null ? other.color : this.color;
        this.createdAt = other.createdAt != null ? other.createdAt : this.createdAt;
        this.deleted = other.deleted != null ? other.deleted : this.deleted;
    }
    public boolean exist() {
        return this.id != null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
