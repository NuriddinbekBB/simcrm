package com.datamond.crm.entity;

import javax.persistence.*;

@Entity
public class Sold {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String productName;

    private int soldquantity;

    private String date;

    private int productId;

    public Sold() {
    }

    public Sold(String productName, int quantity, String date,int productId) {
        this.productName = productName;
        this.soldquantity = quantity;
        this.date = date;
        this.productId=productId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getSoldquantity() {
        return soldquantity;
    }

    public void setSoldquantity(int soldquantity) {
        this.soldquantity = soldquantity;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


}
