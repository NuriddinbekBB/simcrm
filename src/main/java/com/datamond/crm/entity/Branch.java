package com.datamond.crm.entity;

import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Entity
@Table(name = "branch")
public class Branch implements Serializable {
    @Transient
    private static final String sequenceName = "branch_id_seq";

    @Id
    @SequenceGenerator(name = sequenceName, sequenceName = sequenceName, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = sequenceName)
    private Long id;

    @JsonView(DataTablesOutput.View.class)
    @Column(name = "branch_name", length = 1024)
    private String branchName;

    @JsonView(DataTablesOutput.View.class)
    @Column(name = "branch_address", length = 1024)
    private String branchAddress;

    @JsonView(DataTablesOutput.View.class)
    @Column(name = "lattitude")
    private Float lattitude;

    @JsonView(DataTablesOutput.View.class)
    @Column(name = "longtitude")
    private Float longtitude;

    @JsonView(DataTablesOutput.View.class)
    @Column(name = "info")
    private String info;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "region_id", insertable = false, updatable = false)
    @Fetch(FetchMode.JOIN)
    private Region region;

    @Column(name = "region_id")
    private Long regionId;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonView(DataTablesOutput.View.class)
    private Date createdAt;

    private Boolean deleted;

    private Boolean active;

    @PrePersist
    public void onCreate() {
        setCreatedAt(new Date());
        setDeleted(Boolean.FALSE);
        setActive(Boolean.TRUE);
    }
    public void merge(Branch other) {

        this.id = other.id != null ? other.id : this.id;
        this.branchName = other.branchName != null ? other.branchName : this.branchName;
        this.branchAddress = other.branchAddress != null ? other.branchAddress : this.branchAddress;
        this.info = other.info != null ? other.info : this.info;
        this.active = other.active != null ? other.active : this.active;
        this.region = other.region != null ? other.region : this.region;
        this.regionId = other.regionId != null ? other.regionId : this.regionId;
        this.lattitude = other.lattitude != null ? other.lattitude : this.lattitude;
        this.longtitude = other.longtitude != null ? other.longtitude : this.longtitude;
        this.createdAt = other.createdAt != null ? other.createdAt : this.createdAt;
        this.deleted = other.deleted != null ? other.deleted : this.deleted;
    }
    public boolean exist() {
        return this.id != null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchAddress() {
        return branchAddress;
    }

    public void setBranchAddress(String branchAddress) {
        this.branchAddress = branchAddress;
    }

    public Float getLattitude() {
        return lattitude;
    }

    public void setLattitude(Float lattitude) {
        this.lattitude = lattitude;
    }

    public Float getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(Float longtitude) {
        this.longtitude = longtitude;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }
}
