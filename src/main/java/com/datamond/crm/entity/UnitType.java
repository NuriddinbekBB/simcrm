package com.datamond.crm.entity;

public enum UnitType {

    DONA("Dona"),
    KG("Kg"),
    QUTI("Quti"),
    PACHKA("Pachka");

    private final String unitName;

    UnitType(String unitName){
      this.unitName = unitName;
  }
       public String getUnitName(){
        return unitName;
       }

  }
