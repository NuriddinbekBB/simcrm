package com.datamond.crm.entity;

import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Entity
@Table(name = "paynet")
public class Paynet implements Serializable {
    @Transient
    private static final String sequenceName = "paynet_id_seq";

    @Id
    @SequenceGenerator(name = sequenceName, sequenceName = sequenceName, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = sequenceName)
    private Long id;

//    @JsonView(DataTablesOutput.View.class)
//    @Column(name = "branch_address", length = 1024)
//    private String branchAddress;

    @JsonView(DataTablesOutput.View.class)
    @Column(name = "plan")
    private Integer plan;

    @JsonView(DataTablesOutput.View.class)
    @Column(name = "fact")
    private Integer fact;

    @JsonView(DataTablesOutput.View.class)
    @Column(name = "debt")
    private Integer debt;

    @JsonView(DataTablesOutput.View.class)
    @Column(name = "comment")
    private String comment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "branch_id", insertable = false, updatable = false)
    @Fetch(FetchMode.JOIN)
    private Branch branch;

    @Column(name = "branch_id")
    private Long branchId;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonView(DataTablesOutput.View.class)
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonView(DataTablesOutput.View.class)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date givenDate;

    private Boolean deleted;

    private Boolean active;

    @PrePersist
    public void onCreate() {
        setCreatedAt(new Date());
        setDeleted(Boolean.FALSE);
        setActive(Boolean.TRUE);
    }
    public void merge(Paynet other) {

        this.id = other.id != null ? other.id : this.id;
        this.branch = other.branch != null ? other.branch : this.branch;
        this.branchId = other.branchId != null ? other.branchId : this.branchId;
        this.comment = other.comment != null ? other.comment : this.comment;
        this.active = other.active != null ? other.active : this.active;
        this.plan = other.plan != null ? other.plan : this.plan;
        this.fact = other.fact != null ? other.fact : this.fact;
        this.debt = other.debt != null ? other.debt : this.debt;
        this.givenDate = other.givenDate != null ? other.givenDate : this.givenDate;
        this.createdAt = other.createdAt != null ? other.createdAt : this.createdAt;
        this.deleted = other.deleted != null ? other.deleted : this.deleted;
    }
    public boolean exist() {
        return this.id != null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Integer getPlan() {
        return plan;
    }

    public void setPlan(Integer plan) {
        this.plan = plan;
    }

    public Integer getFact() {
        return fact;
    }

    public void setFact(Integer fact) {
        this.fact = fact;
    }

    public Integer getDebt() {
        return debt;
    }

    public void setDebt(Integer debt) {
        this.debt = debt;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    public Date getGivenDate() {
        return givenDate;
    }

    public void setGivenDate(Date givenDate) {
        this.givenDate = givenDate;
    }
}
