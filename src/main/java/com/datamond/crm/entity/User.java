package com.datamond.crm.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "users")
public class User implements Serializable{
    private static final long serialVersionUID = 3214253910554454648L;

    @Transient
    private String role;
    @Transient
    public static final String sequenceName = "users_id_seq";
    @Id
    @SequenceGenerator(name = sequenceName, sequenceName = sequenceName, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = sequenceName)
    @JsonView(DataTablesOutput.View.class)
    private Long id;
    @JsonView(DataTablesOutput.View.class)
    private String username;
    @NotNull
    @JsonView(DataTablesOutput.View.class)
    private String password;
    @JsonView(DataTablesOutput.View.class)
    private Boolean enabled;
    @JsonView(DataTablesOutput.View.class)
    private Boolean deleted = Boolean.FALSE;
    @JsonView(DataTablesOutput.View.class)
    private String firstname;
    @JsonView(DataTablesOutput.View.class)
    private String lastname;
    @JsonView(DataTablesOutput.View.class)
    private String address;
    @JsonView(DataTablesOutput.View.class)
    private String phone;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "branch_id", insertable = false, updatable = false)
    @Fetch(FetchMode.JOIN)
    private Branch branch;

    @Column(name = "branch_id")
    private Long branchId;

    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonProperty("roles")
    private Set<UserRole> roles;
    @PrePersist
    public void onCreate() {
        setEnabled(Boolean.TRUE);
    }

    public boolean exist() {
        return this.id != null;
    }

    public void merge(User other) {
        this.id = other.id != null ? other.id : this.id;
        this.username = other.username != null ? other.username : this.username;
        this.password = other.password != null ? other.password : this.password;
        this.enabled = other.enabled != null ? other.enabled : this.enabled;
        this.deleted = other.deleted != null ? other.deleted : this.deleted;
        this.lastname = other.lastname != null ? other.lastname : this.lastname;
        this.firstname = other.firstname != null ? other.firstname : this.firstname;
        this.roles = other.roles != null ? other.roles : this.roles;
        this.role = other.getRole() != null ? other.getRole() : this.role;
        this.branch = other.branch != null ? other.branch : this.branch;
        this.branchId = other.branchId != null ? other.branchId : this.branchId;
        this.address = other.address != null ? other.address : this.address;
        this.phone = other.phone != null ? other.phone : this.phone;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @JsonIgnore
    @JsonProperty("roles")
    public Set<UserRole> getRoles() {
        return roles;
    }

    @JsonIgnore
    @JsonProperty("roles")
    public void setRoles(Set<UserRole> roles) {
        this.roles = roles;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User entity = (User) o;

        if (!Objects.equals(id, entity.id)) return false;
        if (!Objects.equals(username, entity.username)) return false;
        if (!Objects.equals(password, entity.password)) return false;
        if (!Objects.equals(enabled, entity.enabled)) return false;
        if (!Objects.equals(firstname, entity.firstname)) return false;
        if (!Objects.equals(deleted, entity.deleted)) return false;
        if (!Objects.equals(address, entity.address)) return false;
        if (!Objects.equals(phone, entity.phone)) return false;
        return  (!Objects.equals(lastname, entity.lastname));
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (enabled != null ? enabled.hashCode() : 0);
        result = 31 * result + (deleted != null ? deleted.hashCode() : 0);
        result = 31 * result + (firstname != null ? firstname.hashCode() : 0);
        result = 31 * result + (lastname != null ? lastname.hashCode() : 0);
        return result;
    }

    public User() {
    }
}
