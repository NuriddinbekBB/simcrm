package com.datamond.crm.config;


import com.datamond.crm.constant.Role;
import com.datamond.crm.constant.Urls;
import com.datamond.crm.service.CustomSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {


    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    CustomSuccessHandler customSuccessHandler;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        http.csrf().disable();
        http
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher(Urls.Logout))
                .logoutSuccessUrl(Urls.Login + "?logout")
                .permitAll();
        http
                .formLogin()
                .loginPage(Urls.Login)
                .failureUrl(Urls.Login + "?error")
                .loginProcessingUrl(Urls.Login)
                .defaultSuccessUrl("/dashboard", true)
                .successHandler(customSuccessHandler)
                .permitAll()
                .and()
                .exceptionHandling().accessDeniedPage(Urls.ErrorPage403)
                .and()
                .authorizeRequests()
                .antMatchers("/css/**", "/js/**", "/images/**", "/api/rest/**", "/login", "/lang", "/logout").permitAll()
                .antMatchers("/static/**").permitAll()
                .antMatchers("/lang").permitAll()
                .antMatchers("/webjars/**").permitAll()
                .antMatchers(Urls.UploadFiles + "/**").permitAll()
                .antMatchers(Urls.ErrorPage403).permitAll()
                .antMatchers(Urls.Register).permitAll()
                .antMatchers(Urls.ErrorPage404).permitAll()
                .antMatchers(Urls.ErrorPage500).permitAll()
                //.antMatchers("/dashboard*").permitAll()
                .antMatchers(Urls.UploadFiles + "/**").permitAll()
                .antMatchers(Urls.Admin + "/**").permitAll()
                .antMatchers("/home" + "/**").permitAll()
                .antMatchers("/items" + "/**").permitAll()
                .antMatchers("/products" + "/**").permitAll()
                .antMatchers(Urls.Admin + "/**").hasAnyAuthority(Role.ADMINISTRATOR.name(), Role.MANAGER.name(), Role.BRANCH.name())
                .antMatchers(Urls.Model + "/**").hasAnyAuthority(Role.ADMINISTRATOR.name(), Role.MANAGER.name(), Role.BRANCH.name())
                .anyRequest().denyAll();


    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    protected void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
    }
}
