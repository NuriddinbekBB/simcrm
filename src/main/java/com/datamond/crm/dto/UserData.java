package com.datamond.crm.dto;

import com.datamond.crm.entity.UserRole;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Set;

public class UserData implements Serializable {

    @NotEmpty(message = "First name can not be empty")
    private String firstname;

    @NotEmpty(message = "Last name can not be empty")
    private String lastname;

    @NotEmpty(message = "Last username can not be empty")
    private String username;

    @NotEmpty(message = "Role can not be empty")
    private String role;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonProperty("roles")
    private Set<UserRole> roles;

    @NotEmpty(message = "Phone can not be empty")
    @Email(message = "Please provide a valid phone")
    private String phone;

    @NotEmpty(message = "Password can not be empty")
    private String password;

    //getter and setter


    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @JsonProperty("roles")
    public Set<UserRole> getRoles() {
        return roles;
    }
    @JsonProperty("roles")
    public void setRoles(Set<UserRole> roles) {
        this.roles = roles;
    }
}
