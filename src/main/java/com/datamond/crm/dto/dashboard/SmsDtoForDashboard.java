package com.datamond.crm.dto.dashboard;

public class SmsDtoForDashboard {

    private Long id;
    private String operatorName;
    private Integer count;
    private String color;

    public SmsDtoForDashboard() {
    }

    public SmsDtoForDashboard(String operatorName, Integer count, String color) {
        this.operatorName = operatorName;
        this.count = count;
        this.color = color;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
