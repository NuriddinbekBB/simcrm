package com.datamond.crm.dto.dashboard;

import java.util.Date;

public class SimcardDto {

    private Long id;
    private String operatorName;
    private Date enabledAt;

    public SimcardDto() {
    }

    public SimcardDto(Long id, String operatorName, Date enabledAt) {
        this.id = id;
        this.operatorName = operatorName;
        this.enabledAt = enabledAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public Date getEnabledAt() {
        return enabledAt;
    }

    public void setEnabledAt(Date enabledAt) {
        this.enabledAt = enabledAt;
    }
}
