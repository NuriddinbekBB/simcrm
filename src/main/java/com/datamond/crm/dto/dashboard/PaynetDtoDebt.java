package com.datamond.crm.dto.dashboard;

import java.util.Date;

public class PaynetDtoDebt {

    private Long plan;
    private Long fact;


    public PaynetDtoDebt() {
    }

    public PaynetDtoDebt(Long plan, Long fact) {
        this.plan = plan;
        this.fact = fact;
    }

    public Long getPlan() {
        return plan;
    }

    public void setPlan(Long plan) {
        this.plan = plan;
    }

    public Long getFact() {
        return fact;
    }

    public void setFact(Long fact) {
        this.fact = fact;
    }
}
