package com.datamond.crm.dto.dashboard;

public class PaynetDtoDebtPLan {

    private String name;
    private Long plan;
    private Long fact;


    public PaynetDtoDebtPLan() {
    }

    public PaynetDtoDebtPLan(String name, Long plan, Long fact) {
        this.name = name;
        this.plan = plan;
        this.fact = fact;
    }

    public Long getPlan() {
        return plan;
    }

    public void setPlan(Long plan) {
        this.plan = plan;
    }

    public Long getFact() {
        return fact;
    }

    public void setFact(Long fact) {
        this.fact = fact;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
