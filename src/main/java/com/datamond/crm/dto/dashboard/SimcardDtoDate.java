package com.datamond.crm.dto.dashboard;

import java.util.Date;

public class SimcardDtoDate {

    private Integer count;
    private Date enabledAt;

    public SimcardDtoDate() {
    }

    public SimcardDtoDate(Integer count, Date enabledAt) {
        this.count = count;
        this.enabledAt = enabledAt;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Date getEnabledAt() {
        return enabledAt;
    }

    public void setEnabledAt(Date enabledAt) {
        this.enabledAt = enabledAt;
    }
}
