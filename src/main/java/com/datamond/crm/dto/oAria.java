package com.datamond.crm.dto;

public class oAria {
    private String sSortAscending;
    private String sSortDescending;

    public String getsSortAscending() {
        return sSortAscending;
    }

    public void setsSortAscending(String sSortAscending) {
        this.sSortAscending = sSortAscending;
    }

    public String getsSortDescending() {
        return sSortDescending;
    }

    public void setsSortDescending(String sSortDescending) {
        this.sSortDescending = sSortDescending;
    }
}
